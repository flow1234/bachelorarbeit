import naiveBayes
import Fingerprint

def jackknife():
	
	# No Direction
	bayes = naiveBayes.Filter('0')
	trainingsData = bayes.fingerprints
	
	# With Direction
	#bayes1 = naiveBayes.Filter('1')
	#trainingsData = bayes1.fingerprints
	allFingerprints = len(trainingsData)

	testObject = []

	macs = []
	rooms = []

	for fp in trainingsData:

		if fp.room not in macs:

			macs.append(fp.room)
			rooms.append(Room(fp.room))

	for i in range(len(trainingsData)):

		testObject.append(trainingsData.pop(0))

		# With Direction
		#bayes = naiveBayes.Filter(str(testObject[0].direction))	
	
		bayes.fingerprints = trainingsData

		print "\nFingerprint (%d/%d) - %s" % (i+1, allFingerprints, testObject[0].room)

		for wifi_scan in testObject[0].wifi_scans:

			result = bayes.getRoom(wifi_scan.ap_infos)

			print "__Scan (%d/20) - %s" % (wifi_scan.number,result)

			for room in rooms:

				if testObject[0].room == room.name:

					room.list.append(result)

					if room.name == result:

						room.true += 1 # classified correct

					else:

						room.false += 1 # classified wrong

		trainingsData.append(testObject.pop(0))

	for room in rooms:

		print "Room %s , Correct = %d , Wrong = %d" % (room.name,room.true,room.false)

def main():

	jackknife()

class Room:
	def __init__(self,name):
		self.name  = name
		self.true  = 0
		self.false = 0
		self.list = []

main()
