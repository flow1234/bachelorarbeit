import naiveBayes
import Fingerprint

# positions (initial Point and direction)
fps = []
fps.append(("hall",'3'))  # 1 - hall
fps.append(("corL2",'4')) # 2 - corL2
fps.append(("g002",'2'))  # 3 - G002
fps.append(("corL1",'2')) # 4 - corL1
fps.append(("corU2",'1')) # 5 - corU2
fps.append(("g002",'4'))  # 6 - G002
fps.append(("g009",'2'))  # 7 - G009
fps.append(("g008",'1'))  # 8 - G008
fps.append(("g006",'2'))  # 9 - G006
fps.append(("g003",'2'))  # 10 - G003
fps.append(("g001",'2'))  # 11 - G001 
fps.append(("corL2",'1')) # 12 - corL2
fps.append(("corL1",'4')) # 13 - corL1
fps.append(("corU2",'1')) # 14 - corU2
fps.append(("g010",'2'))  # 15 - G010
fps.append(("g010",'3'))  # 16 - G010 
fps.append(("g010",'1'))  # 17 - G010
fps.append(("g004",'3'))  # 18 - G004
fps.append(("corU3",'2')) # 19 - corU3

def readTxtFile(scantype,fpNr):
	""" get all scans from .txt-File """

	if scantype == 0: # active

		f = open("Data/RF_afp"+str(fpNr)+".txt",'r')

	else: # passive

		f = open("Data/RF_bfp"+str(fpNr)+".txt",'r')

	measures_raw = []
	measures_final = []
	macs = []

	for line in f:

		measures_raw.append(Fingerprint.APInfo(line.split(',')[2].rstrip('\n'),int(line.split(',')[1])))

	for measure in measures_raw:

		if measure.mac[0:14] not in macs:

			macs.append(measure.mac[0:14])

	for mac in macs:

		sumRSSI = 0
		ctr = 0

		for measure in measures_raw:

			if measure.mac[0:14] == mac:

				sumRSSI += measure.rssi
				ctr += 1

		measures_final.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

	return measures_final


def main():
	""" Main Function """

	# for every Point
	for j in range(len(fps)):	
		activeScan  = readTxtFile(0,j+1)
		passiveScan = readTxtFile(1,j+1)

		nBayes = naiveBayes.Filter(fps[j][1])

		print "\nFingerprint %d" % (j+1)
		print "Expected Room: %s" % (fps[j][0])
		print "Active:", nBayes.getRoom(activeScan)
		print "Passive:", nBayes.getRoom(passiveScan)

main()
