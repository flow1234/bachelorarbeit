from matplotlib import pyplot as plt
from matplotlib import patches as mpatches
import cv2

class Instance(object):

	def __init__(self):

		# Constants
		self.INITIAL_HEIGHT = 439
		self.INITIAL_WIDTH  = 704

		# load Image and get Size		
		self.img = self.loadIMG()
		self.height = self.img.shape[0]
		self.width = self.img.shape[1]
	
		self.iPt = None
		self.aPt = None
		self.bPt = None

		# plot Image
		plt.imshow(self.img, cmap = 'gray', interpolation = 'bicubic')
		plt.xticks([]), plt.yticks([])

	def loadIMG(self): return cv2.imread('Map/plan.jpg',0)

	def scaleX(self, x): return x * self.width / self.INITIAL_WIDTH

	def scaleY(self, y): return y * self.height / self.INITIAL_HEIGHT

	def setDBPoints(self, x, y):

		if type(x) is list and type(y) is list and len(x) == len(y):

			xValues = [ self.scaleX(coord) for coord in x ]
			yValues = [ self.scaleY(coord) for coord in y ]
			plt.scatter(xValues,yValues,color='b')

		elif type(x) is not list and type(y) is not list:

			plt.scatter(self.scaleX(x),self.scaleY(y),color='b')

		else:

			print "Error at setPoints. Incompatible Types"


	def setLivePoints(self, x, y):

		if type(x) is list and type(y) is list and len(x) == len(y):

			xValues = [ self.scaleX(coord) for coord in x ]
			yValues = [ self.scaleY(coord) for coord in y ]
			plt.scatter(xValues,yValues,color='r')

		elif type(x) is not list and type(y) is not list:

			plt.scatter(self.scaleX(x),self.scaleY(y),color='r')

		else:

			print "Error at setPoints. Incompatible Types"

	def setSmartposPoints(self, dbX, dbY, liveX, liveY):

			dbX_Values   = [ self.scaleX(coord) for coord in dbX ]
			dbY_Values   = [ self.scaleY(coord) for coord in dbY ]
			liveX_Values = [ self.scaleX(coord) for coord in liveX ]
			liveY_Values = [ self.scaleY(coord) for coord in liveY ]

			plt.scatter(dbX_Values,dbY_Values,color='k',alpha=0.4,s=55)
			plt.scatter(liveX_Values,liveY_Values,color='k',s=55)

	def show(self): 

		#plt.savefig('Map.png', format='png', dpi=1200, bbox_inches='tight', pad_inches=0)
		plt.show()
