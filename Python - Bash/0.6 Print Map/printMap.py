import Map
import sys
import sqlite3 as lite

dbXCoords = []
dbYCoords = []
xCoords = []
yCoords = []

def loadDBCoords():
	""" Coordinates from Database """

	try:

		con = lite.connect('Database/DBFinal.sqlite')

		cur = con.cursor()

		cur.execute("SELECT * FROM deterministic_fingerprints WHERE direction = 1")

		data = cur.fetchall()

		for entry in data:

			dbXCoords.append(entry[1])
			dbYCoords.append(entry[2])

	except lite.Error, e:

		print "Error %s:" % e.args[0]
		sys.exit(1)

	finally:

		if con:

			con.close()
	

def loadLiveCoords():
	""" Coordinates from Live Scans """

	xCoords.append(358); yCoords.append(93)
	xCoords.append(359); yCoords.append(322)
	xCoords.append(299); yCoords.append(125)
	xCoords.append(359); yCoords.append(202)
	xCoords.append(406); yCoords.append(36)
	xCoords.append(326); yCoords.append(124)
	xCoords.append(405); yCoords.append(381)
	xCoords.append(319); yCoords.append(295)
	xCoords.append(323); yCoords.append(249)
	xCoords.append(417); yCoords.append(190)
	xCoords.append(354); yCoords.append(406)
	xCoords.append(363); yCoords.append(264)
	xCoords.append(398); yCoords.append(146)
	xCoords.append(292); yCoords.append(32)
	xCoords.append(312); yCoords.append(399)
	xCoords.append(329); yCoords.append(380)
	xCoords.append(302); yCoords.append(374)
	xCoords.append(326); yCoords.append(195)
	xCoords.append(591); yCoords.append(31)


def showHelp():
	""" show Help """

	print "\nError. You have to give a parameter:\n"
	print "1 - show Database Map"
	print "2 - show LiveScans Map\n"


def main():
	""" Main Function """

	if len(sys.argv) != 2 or int(sys.argv[1]) < 1 or int(sys.argv[1]) > 3:

		showHelp()

	else:

		myMap = Map.Instance()

		if (int(sys.argv[1])==1):
			
			loadDBCoords()
			myMap.setDBPoints(dbXCoords,dbYCoords)

		elif (int(sys.argv[1])==2):

			loadLiveCoords()
			myMap.setLivePoints(xCoords,yCoords)

		else:

			loadDBCoords()
			loadLiveCoords()
			myMap.setSmartposPoints(dbXCoords,dbYCoords,xCoords,yCoords)
			
		myMap.show()

main()
