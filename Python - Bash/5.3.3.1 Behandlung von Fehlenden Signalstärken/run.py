import sys
import DistanceReasoner
import Fingerprint
import math
import matplotlib.pyplot as plt
import datetime

# positions (initial Point and direction)
fps = []
fps.append((Fingerprint.Point(358,93),'3'))  # 1
fps.append((Fingerprint.Point(359,322),'4')) # 2
fps.append((Fingerprint.Point(299,125),'2')) # 3
fps.append((Fingerprint.Point(359,202),'2')) # 4
fps.append((Fingerprint.Point(406,36),'1'))  # 5
fps.append((Fingerprint.Point(326,124),'4')) # 6
fps.append((Fingerprint.Point(405,381),'2')) # 7
fps.append((Fingerprint.Point(319,295),'1')) # 8
fps.append((Fingerprint.Point(323,249),'2')) # 9
fps.append((Fingerprint.Point(417,190),'2')) # 10
fps.append((Fingerprint.Point(398,146),'2')) # 11
fps.append((Fingerprint.Point(354,406),'1')) # 12
fps.append((Fingerprint.Point(363,264),'4')) # 13
fps.append((Fingerprint.Point(292,32),'1'))  # 14
fps.append((Fingerprint.Point(312,399),'2')) # 15
fps.append((Fingerprint.Point(329,380),'3')) # 16
fps.append((Fingerprint.Point(302,374),'1')) # 17
fps.append((Fingerprint.Point(326,195),'3')) # 18
fps.append((Fingerprint.Point(591,31),'2'))  # 19

maximum_k = 15
scaleMeter = 13.0

# arrays with results for each value of k
# [kValue][meanPosError, maxPosError, minPosError, stdDev]
# CO ~ Consider a Min Value
# NO ~ No Min Value
resultsActive_CO = [[0 for x in range(4)] for x in range(maximum_k)]
resultsActive_NO = [[0 for x in range(4)] for x in range(maximum_k)]

resultsPassive_CO = [[0 for x in range(4)] for x in range(maximum_k)]
resultsPassive_NO = [[0 for x in range(4)] for x in range(maximum_k)]


def fetchActiveData(fpNr):
	""" get all scans from .txt-File """

	f = open("Data/RF_afp"+str(fpNr)+".txt",'r')

	measures_raw = []
	measures_final = []
	macs = []

	for line in f:

		measures_raw.append(Fingerprint.APInfo(line.split(',')[2].rstrip('\n'),int(line.split(',')[1])))

	for measure in measures_raw:

		if measure.mac[0:14] not in macs:

			macs.append(measure.mac[0:14])

	for mac in macs:

		sumRSSI = 0
		ctr = 0

		for measure in measures_raw:

			if measure.mac[0:14] == mac:

				sumRSSI += measure.rssi
				ctr += 1

		measures_final.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

	return measures_final


def fetchPassiveData(fpNr):

	f = open("Data/RF_bfp"+str(fpNr)+".txt",'r')

	dataset1 = []

	ap_infos = []
	scanctr = 1
	scans = []

	timestamp    = f.readline().split(',')[0]

	hours        = int(timestamp.split(':')[0])
	minutes      = int(timestamp.split(':')[1])
	seconds      = int(timestamp.split(':')[2].split('.')[0])
	milliseconds = int(timestamp.split(':')[2].split('.')[1])

	START = datetime.datetime(2015,6,20,hours,minutes,seconds,milliseconds)

	for line in f:

		# handle timestamps
		timestamp    = line.split(',')[0]

		hours        = int(timestamp.split(':')[0])
		minutes      = int(timestamp.split(':')[1])
		seconds      = int(timestamp.split(':')[2].split('.')[0])
		milliseconds = int(timestamp.split(':')[2].split('.')[1])

		NOW = datetime.datetime(2015,6,20,hours,minutes,seconds,milliseconds)

		ap_infos.append(Fingerprint.APInfo(line.split(',')[2].rstrip('\n'),int(line.split(',')[1])))

		if (NOW > (START+datetime.timedelta(seconds=1))):

			START = NOW
			scans.append(Fingerprint.Scan(scanctr,ap_infos))

			ap_infos = []
			scanctr += 1

	# Calculate Dataset 1: 3 Seconds tcpump - dynamic user (Take 1 Second from first Capture of each Channel)

	d1_macs = []
	d1_aplist_raw = []
	d1_aplist_fin = []

	# We swapped Channels every 2 Seconds

	for ap_info in scans[0].ap_infos: # Channel 1

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for ap_info in scans[3].ap_infos: # Channel 6

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for ap_info in scans[5].ap_infos: # Channel 11

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for mac in d1_macs:

		sumRSSI = 0
		ctr = 0

		for ap_info in d1_aplist_raw:

			if ap_info.mac == mac:

				sumRSSI += ap_info.rssi
				ctr += 1

		d1_aplist_fin.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

	dataset1.append(Fingerprint.Scan(scans[0].number,d1_aplist_fin))

	return dataset1[0].ap_infos


def getValues():
	""" Calculate Mean/Max Positioning Error and Standard Deviation for different Values of K """

	distancesActive_CO   = [[0 for x in range(len(fps))] for x in range(maximum_k)] 
	distancesActive_NO   = [[0 for x in range(len(fps))] for x in range(maximum_k)]

	distancesPassive_CO  = [[0 for x in range(len(fps))] for x in range(maximum_k)] 
	distancesPassive_NO  = [[0 for x in range(len(fps))] for x in range(maximum_k)]

	# for every kValue < 14
	for i in range(maximum_k):

		# for every Point
		for j in range(len(fps)):

			# Read the .txt-File
			activeScan  = fetchActiveData(j+1)
			passiveScan = fetchPassiveData(j+1)

			# Initialize a Distance Reasoner based on kValue and Direction of certain Point
			# True - Assign a Min Value
			# False - No Min Value
			ddr_CO = DistanceReasoner.Deterministic(fps[j][1],i+1,True)
			ddr_NO = DistanceReasoner.Deterministic(fps[j][1],i+1,False)

			# Get the Approximate Position
			activePt_CO  = ddr_CO.getApproximatePosition(activeScan)
			activePt_NO  = ddr_NO.getApproximatePosition(activeScan)

			passivePt_CO = ddr_CO.getApproximatePosition(passiveScan)
			passivePt_NO = ddr_NO.getApproximatePosition(passiveScan)

			# Calculate Euclidean Distances
			distancesActive_CO[i][j] = math.sqrt(math.pow(fps[j][0].x-activePt_CO.x,2)+math.pow(fps[j][0].y-activePt_CO.y,2))
			distancesActive_NO[i][j] = math.sqrt(math.pow(fps[j][0].x-activePt_NO.x,2)+math.pow(fps[j][0].y-activePt_NO.y,2))

			distancesPassive_CO[i][j] = math.sqrt(math.pow(fps[j][0].x-passivePt_CO.x,2)+math.pow(fps[j][0].y-passivePt_CO.y,2))
			distancesPassive_NO[i][j] = math.sqrt(math.pow(fps[j][0].x-passivePt_NO.x,2)+math.pow(fps[j][0].y-passivePt_NO.y,2))


	# Calculate the desired Values
	for i in range(maximum_k):

		# Average Positioning Error
		meanPosError_active_CO  = sum(distancesActive_CO[i])/len(distancesActive_CO[i])
		meanPosError_active_NO  = sum(distancesActive_NO[i])/len(distancesActive_NO[i])

		meanPosError_passive_CO = sum(distancesPassive_CO[i])/len(distancesPassive_CO[i])
		meanPosError_passive_NO = sum(distancesPassive_NO[i])/len(distancesPassive_NO[i])

		# Maximum Positioning Error
		maxPosError_active_CO = max(distancesActive_CO[i])
		maxPosError_active_NO = max(distancesActive_NO[i])

		maxPosError_passive_CO = max(distancesPassive_CO[i])
		maxPosError_passive_NO = max(distancesPassive_NO[i])

		# Minimum Positioning Error
		minPosError_active_CO = min(distancesActive_CO[i])
		minPosError_active_NO = min(distancesActive_NO[i])

		minPosError_passive_CO = min(distancesPassive_CO[i])
		minPosError_passive_NO = min(distancesPassive_NO[i])

		# Standard Deviation
		s2_active_CO  = 0
		s2_active_NO  = 0

		s2_passive_CO = 0
		s2_passive_NO = 0

		for j in range(len(distancesActive_CO[i])):

			s2_active_CO += math.pow(distancesActive_CO[i][j]-meanPosError_active_CO,2)
			s2_active_NO += math.pow(distancesActive_NO[i][j]-meanPosError_active_NO,2)

			s2_passive_CO += math.pow(distancesPassive_CO[i][j]-meanPosError_passive_CO,2)
			s2_passive_NO += math.pow(distancesPassive_NO[i][j]-meanPosError_passive_NO,2)
		
		standardDeviation_active_CO = math.sqrt(s2_active_CO/len(fps))
		standardDeviation_active_NO = math.sqrt(s2_active_NO/len(fps))

		standardDeviation_passive_CO = math.sqrt(s2_passive_CO/len(fps))
		standardDeviation_passive_NO = math.sqrt(s2_passive_NO/len(fps))

		# Store Results
		resultsActive_CO[i][0] = meanPosError_active_CO / scaleMeter
		resultsActive_CO[i][1] = maxPosError_active_CO / scaleMeter
		resultsActive_CO[i][2] = minPosError_active_CO / scaleMeter
		resultsActive_CO[i][3] = standardDeviation_active_CO / scaleMeter

		resultsActive_NO[i][0] = meanPosError_active_NO / scaleMeter
		resultsActive_NO[i][1] = maxPosError_active_NO / scaleMeter
		resultsActive_NO[i][2] = minPosError_active_NO / scaleMeter
		resultsActive_NO[i][3] = standardDeviation_active_NO / scaleMeter

		resultsPassive_CO[i][0] = meanPosError_passive_CO / scaleMeter
		resultsPassive_CO[i][1] = maxPosError_passive_CO / scaleMeter
		resultsPassive_CO[i][2] = minPosError_passive_CO / scaleMeter
		resultsPassive_CO[i][3] = standardDeviation_passive_CO / scaleMeter

		resultsPassive_NO[i][0] = meanPosError_passive_NO / scaleMeter
		resultsPassive_NO[i][1] = maxPosError_passive_NO / scaleMeter
		resultsPassive_NO[i][2] = minPosError_passive_NO / scaleMeter
		resultsPassive_NO[i][3] = standardDeviation_passive_NO / scaleMeter


def plotResults(inputmode):

	kValues = []
	mode = inputmode

	for i in range(maximum_k):

		kValues.append((i+1))		

	meanPosErrors_active_CO  = []
	meanPosErrors_active_NO  = []
	meanPosErrors_passive_CO = []
	meanPosErrors_passive_NO = []

	maxPosErrors_active_CO  = []
	maxPosErrors_active_NO  = []
	maxPosErrors_passive_CO = []
	maxPosErrors_passive_NO = []

	minPosErrors_active_CO  = []
	minPosErrors_active_NO  = []
	minPosErrors_passive_CO = []
	minPosErrors_passive_NO = []

	stdDev_active_CO  = []
	stdDev_active_NO  = []
	stdDev_passive_CO = []
	stdDev_passive_NO = []
	
	for i in range(len(resultsActive_CO)):

		meanPosErrors_active_CO.append(resultsActive_CO[i][0])
		meanPosErrors_active_NO.append(resultsActive_NO[i][0])
		meanPosErrors_passive_CO.append(resultsPassive_CO[i][0])
		meanPosErrors_passive_NO.append(resultsPassive_NO[i][0])

		maxPosErrors_active_CO.append(resultsActive_CO[i][1])
		maxPosErrors_active_NO.append(resultsActive_NO[i][1])
		maxPosErrors_passive_CO.append(resultsPassive_CO[i][1])
		maxPosErrors_passive_NO.append(resultsPassive_NO[i][1])

		minPosErrors_active_CO.append(resultsActive_CO[i][2])
		minPosErrors_active_NO.append(resultsActive_NO[i][2])
		minPosErrors_passive_CO.append(resultsPassive_CO[i][2])
		minPosErrors_passive_NO.append(resultsPassive_NO[i][2])

		stdDev_active_CO.append(resultsActive_CO[i][3])
		stdDev_active_NO.append(resultsActive_NO[i][3])
		stdDev_passive_CO.append(resultsPassive_CO[i][3])
		stdDev_passive_NO.append(resultsPassive_NO[i][3])


	for i in range(maximum_k):

		print "K=%d" % (i+1)
		print "Active_CO : min=%.2f m, mean=%.2f m, max=%.2f m, stdDev=%.2f m" % (minPosErrors_active_CO[i],meanPosErrors_active_CO[i],maxPosErrors_active_CO[i],stdDev_active_CO[i])
		print "Active_NO : min=%.2f m, mean=%.2f m, max=%.2f m, stdDev=%.2f m" % (minPosErrors_active_NO[i],meanPosErrors_active_NO[i],maxPosErrors_active_NO[i],stdDev_active_NO[i])
		print "Passive_CO : min=%.2f m, mean=%.2f m, max=%.2f m, stdDev=%.2f m" % (minPosErrors_passive_CO[i],meanPosErrors_passive_CO[i],maxPosErrors_passive_CO[i],stdDev_passive_CO[i])
		print "Passive_NO : min=%.2f m, mean=%.2f m, max=%.2f m, stdDev=%.2f m" % (minPosErrors_passive_NO[i],meanPosErrors_passive_NO[i],maxPosErrors_passive_NO[i],stdDev_passive_NO[i])

	plt.xlabel('k')
	plt.ylabel('Positionierungsfehler (in Meter)')
	plt.xlim([0,16])
	plt.ylim([0,35.0])
	plt.grid()


	if (mode == 1): # Active Standard Deviation
		plt.plot(kValues, stdDev_active_CO, label="Mit MinWert", linewidth=3.,color='r')
		plt.plot(kValues, stdDev_active_NO, label="Ohne MinWert", linewidth=3.,color='r', linestyle='dashed')
		plt.ylabel('Standardabweichung (in Meter)')
		plt.ylim([0,10.0])
		plt.legend()
		plt.savefig('MinVal-StdDev-A.pdf', format='pdf', dpi=1200)

	elif (mode == 2): # Active With MinValue
		plt.plot(kValues, meanPosErrors_active_CO, label="Durchschnitt", linewidth=3.,color='r')
		plt.plot(kValues, maxPosErrors_active_CO, label="Minimum/Maximum", linewidth=3.,color='k', linestyle='dashed')
		plt.plot(kValues, minPosErrors_active_CO, linewidth=3.,color='k', linestyle='dashed')
		#plt.title("Active Scans - gewichtet")
		plt.legend()
		plt.savefig('Min-A.pdf', format='pdf', dpi=1200)

	elif (mode == 3): # Active No MinValue
		plt.plot(kValues, meanPosErrors_active_NO, label="Durchschnitt", linewidth=3.,color='r')
		plt.plot(kValues, minPosErrors_active_NO, label="Minimum/Maximum", linewidth=3.,color='k', linestyle='dashed')
		plt.plot(kValues, maxPosErrors_active_NO, linewidth=3.,color='k', linestyle='dashed')
		#plt.title("Active Scans - nicht gewichtet")
		plt.legend()
		plt.savefig('NoMin-A.pdf', format='pdf', dpi=1200)

	if (mode == 4): # Passive Standard Deviation
		plt.plot(kValues, stdDev_passive_CO, label="Mit MinWert", linewidth=3.,color='g')
		plt.plot(kValues, stdDev_passive_NO, label="Ohne MinWert", linewidth=3.,color='g', linestyle='dashed')
		plt.ylabel('Standardabweichung (in Meter)')
		plt.ylim([0,10.0])
		plt.legend()
		plt.savefig('MinVal-StdDev-P.pdf', format='pdf', dpi=1200)

	elif (mode == 5): # Passive With MinValue
		plt.plot(kValues, meanPosErrors_passive_CO, label="Durchschnitt", linewidth=3.,color='g')
		plt.plot(kValues, maxPosErrors_passive_CO, label="Minimum/Maximum", linewidth=3.,color='k', linestyle='dashed')
		plt.plot(kValues, minPosErrors_passive_CO, linewidth=3.,color='k', linestyle='dashed')
		#plt.title("Passive Scans - gewichtet")
		plt.legend()
		plt.savefig('Min-P.pdf', format='pdf', dpi=1200)

	elif (mode == 6): # Passive No MinValue
		plt.plot(kValues, meanPosErrors_passive_NO, label="Durchschnitt", linewidth=3.,color='g')
		plt.plot(kValues, minPosErrors_passive_NO, label="Minimum/Maximum", linewidth=3.,color='k', linestyle='dashed')
		plt.plot(kValues, maxPosErrors_passive_NO, linewidth=3.,color='k', linestyle='dashed')
		#plt.title("Passive Scans - nicht gewichtet")
		plt.legend()
		plt.savefig('NoMin-P.pdf', format='pdf', dpi=1200)


	#plt.xlabel('k')
	#plt.legend()
	#plt.xlim([1,14])
	#plt.ylim([0,30.0])
	#plt.savefig('myimage.svg', format='svg', dpi=1200)
	plt.show()


def showHelp():
	""" show Help """

	print "\nError. Need Parameter for mode:\n"
	print "(1 - Active  - Standardabweichung)"
	print "(2 - Active  - Mit Min-Wert)"
	print "(3 - Active  - Ohne Min-Wert)"
	print "(4 - Passive - Standardabweichung)"
	print "(5 - Passive - Mit Min-Wert)"
	print "(6 - Passive - Ohne Min-Wert)"


def main():
	""" Main Function """

	if len(sys.argv) != 2 or int(sys.argv[1]) < 1 or int(sys.argv[1]) > 6:

		showHelp()

	else:

		getValues()
		plotResults(int(sys.argv[1]))

main()
