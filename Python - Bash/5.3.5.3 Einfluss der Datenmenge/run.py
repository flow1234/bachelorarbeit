from matplotlib import pyplot as plt
from matplotlib import dates as dt
import datetime
import sys
import math

import DistanceReasoner
import Fingerprint


timestamps = []
scaleMeter = 13.0

dataset1 = []
dataset2 = []
dataset3 = []

distances_d1 = []
distances_d2 = []
distances_d3 = []


def fetchData():
	
	f = open('Data/'+sys.argv[1]+'.txt','r')

	print "Reading Document"

	ap_infos = []
	scanctr = 1
	scans = []

	timestamp    = f.readline().split(',')[0]

	hours        = int(timestamp.split(':')[0])
	minutes      = int(timestamp.split(':')[1])
	seconds      = int(timestamp.split(':')[2].split('.')[0])
	milliseconds = int(timestamp.split(':')[2].split('.')[1])

	START = datetime.datetime(2015,6,20,hours,minutes,seconds,milliseconds)
	TS = datetime.datetime(2015,6,20,hours,minutes,seconds,milliseconds)

	for line in f:

		# handle timestamps
		timestamp    = line.split(',')[0]

		hours        = int(timestamp.split(':')[0])
		minutes      = int(timestamp.split(':')[1])
		seconds      = int(timestamp.split(':')[2].split('.')[0])
		milliseconds = int(timestamp.split(':')[2].split('.')[1])

		NOW = datetime.datetime(2015,6,20,hours,minutes,seconds,milliseconds)

		ap_infos.append(Fingerprint.APInfo(line.split(',')[2].rstrip('\n'),int(line.split(',')[1])))

		if (NOW > (START+datetime.timedelta(seconds=1))):

			START = NOW

			if (scanctr % 20 == 0):

				timestamps.append(NOW)
				TS = NOW
	
			scans.append(Fingerprint.Scan(scanctr,ap_infos))

			ap_infos = []
			scanctr += 1


	# Always use Data of 3 Seconds for a Calculation
	d1_macs = []
	d1_aplist_raw = []
	d1_aplist_fin = []

	# Always use Data of 20 Seconds for a Calculation
	d2_macs = []
	d2_aplist_raw = []
	d2_aplist_fin = []

	# Always use whole Data for all Calculations
	d3_macs = []
	d3_aplist_raw = []
	d3_aplist_fin = []

	final_ctr = 0

	for scan in scans:

		if (scan.number % 20) == 0: # if 20 Scans passed

			for ap_info in scan.ap_infos: # Get Remaining Measures

				if ap_info.mac[0:14] not in d1_macs:

					d1_macs.append(ap_info.mac[0:14])

				if ap_info.mac[0:14] not in d2_macs:

					d2_macs.append(ap_info.mac[0:14])

				if ap_info.mac[0:14] not in d3_macs:

					d3_macs.append(ap_info.mac[0:14])

				d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14], ap_info.rssi))
				d2_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14], ap_info.rssi))
				d3_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14], ap_info.rssi))

			# Calculate Average of Measures for each Scan and add Results
			for mac in d1_macs:

				sumRSSI = 0
				ctr = 0

				for ap_info in d1_aplist_raw:

					if ap_info.mac == mac:

						sumRSSI += ap_info.rssi
						ctr += 1

				d1_aplist_fin.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

			for mac in d2_macs:

				sumRSSI = 0
				ctr = 0

				for ap_info in d2_aplist_raw:

					if ap_info.mac == mac:

						sumRSSI += ap_info.rssi
						ctr += 1

				d2_aplist_fin.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

			for mac in d3_macs:

				sumRSSI = 0
				ctr = 0

				for ap_info in d3_aplist_raw:

					if ap_info.mac == mac:

						sumRSSI += ap_info.rssi
						ctr += 1

				d3_aplist_fin.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

			# Copy Data in Final list
			d1_list = list(d1_aplist_fin)
			dataset1.append(Fingerprint.Scan(final_ctr,d1_list))
			d2_list = list(d2_aplist_fin)
			dataset2.append(Fingerprint.Scan(final_ctr,d2_list))
			d3_list = list(d3_aplist_fin)
			dataset3.append(Fingerprint.Scan(final_ctr,d3_list))

			# Remove old Data - Keep Data for dataset3
			del d1_macs[:]
			del d1_aplist_raw[:]
			del d1_aplist_fin[:]

			del d2_macs[:]
			del d2_aplist_raw[:]
			del d2_aplist_fin[:]

			final_ctr += 1

		else: # If we're in between 20 Scans

			for ap_info in scan.ap_infos:

				if (((scan.number % 20) + 9) == 9) or (((scan.number % 20) + 16) == 16):

					if ap_info.mac[0:14] not in d1_macs:

						d1_macs.append(ap_info.mac[0:14])

					d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14], ap_info.rssi))

				if ap_info.mac[0:14] not in d2_macs:

					d2_macs.append(ap_info.mac[0:14])

				if ap_info.mac[0:14] not in d3_macs:

					d3_macs.append(ap_info.mac[0:14])

				d2_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14], ap_info.rssi))
				d3_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14], ap_info.rssi))



def handleData():

	# (the position where the beacon got captured) G008 - Dir 2
	if sys.argv[1] == 'g008':

		initP = Fingerprint.Point(315,300)
		ddr = DistanceReasoner.Deterministic('2', 4)

	# (the position where the beacon got captured) G003 - Dir 3
	elif sys.argv[1] == 'g003':

		initP = Fingerprint.Point(407,195)
		ddr = DistanceReasoner.Deterministic('3', 4)


	for i in range(len(dataset1)):

		calcP_d1 = ddr.getApproximatePosition(dataset1[i].ap_infos)
		calcP_d2 = ddr.getApproximatePosition(dataset2[i].ap_infos)
		calcP_d3 = ddr.getApproximatePosition(dataset3[i].ap_infos)

		distance = math.sqrt(math.pow(initP.x-calcP_d1.x,2)+math.pow(initP.y-calcP_d1.y,2))
		distances_d1.append(distance/scaleMeter)
		distance = math.sqrt(math.pow(initP.x-calcP_d2.x,2)+math.pow(initP.y-calcP_d2.y,2))
		distances_d2.append(distance/scaleMeter)
		distance = math.sqrt(math.pow(initP.x-calcP_d3.x,2)+math.pow(initP.y-calcP_d3.y,2))
		distances_d3.append(distance/scaleMeter)

	print "Average d1: ", (sum(distances_d1)/len(distances_d1))
	print "Average d2: ", (sum(distances_d2)/len(distances_d2))
	print "Average d3: ", (sum(distances_d3)/len(distances_d3))
	print "Max d1: ", max(distances_d1)
	print "Max d2: ", max(distances_d2)
	print "Max d3: ", max(distances_d3)



def plotResults():

	# define labels
#	plt.title('Beacon Positioning Accuracy')
	plt.ylabel('Positionierungsfehler in Meter')
	plt.xlabel('Zeit')

	# set limits
	xmin = dt.date2num(timestamps[0])
	xmax = dt.date2num(timestamps[len(timestamps)-1])
	plt.xlim(xmin,xmax)
	plt.ylim(0,8)

	# plot values
	plt.plot(timestamps,distances_d1,c='k',label='Dynamisch',linewidth=3.,alpha=0.5)
	plt.plot(timestamps,distances_d2,c='r',label='Statisch',linewidth=3.,alpha=0.8, linestyle='dashed')
	plt.plot(timestamps,distances_d3,c='g',label='Gesamtdaten',linewidth=3.,linestyle='dashed')

	# format x-axis representation
	plt.gca().xaxis.set_major_formatter(dt.DateFormatter("%H:%M:%S"))
	plt.grid()
	plt.legend()

	# showing what we plotted
	plt.show()



def main():

	fetchData()
	handleData()	
	plotResults()
	


# run script
main()
