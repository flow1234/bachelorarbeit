import sys
import DistanceReasoner
import Fingerprint
import Map
import math
import datetime

# positions (initial Point and direction)
fps = []
fps.append((Fingerprint.Point(358,93),'3'))  # 1
fps.append((Fingerprint.Point(359,322),'4')) # 2
fps.append((Fingerprint.Point(299,125),'2')) # 3
fps.append((Fingerprint.Point(359,202),'2')) # 4
fps.append((Fingerprint.Point(406,36),'1'))  # 5
fps.append((Fingerprint.Point(326,124),'4')) # 6
fps.append((Fingerprint.Point(405,381),'2')) # 7
fps.append((Fingerprint.Point(319,295),'1')) # 8
fps.append((Fingerprint.Point(323,249),'2')) # 9
fps.append((Fingerprint.Point(417,190),'2')) # 10
fps.append((Fingerprint.Point(398,146),'2')) # 11
fps.append((Fingerprint.Point(354,406),'1')) # 12
fps.append((Fingerprint.Point(363,264),'4')) # 13
fps.append((Fingerprint.Point(292,32),'1'))  # 14
fps.append((Fingerprint.Point(312,399),'2')) # 15
fps.append((Fingerprint.Point(329,380),'3')) # 16
fps.append((Fingerprint.Point(302,374),'1')) # 17
fps.append((Fingerprint.Point(326,195),'3')) # 18
fps.append((Fingerprint.Point(591,31),'2'))  # 19


def fetchActiveData(fpNr):
	""" get all scans from .txt-File """

	f = open("Data/RF_afp"+str(fpNr)+".txt",'r')

	measures_raw = []
	measures_final = []
	macs = []

	for line in f:

		measures_raw.append(Fingerprint.APInfo(line.split(',')[2].rstrip('\n'),int(line.split(',')[1])))

	for measure in measures_raw:

		if measure.mac[0:14] not in macs:

			macs.append(measure.mac[0:14])

	for mac in macs:

		sumRSSI = 0
		ctr = 0

		for measure in measures_raw:

			if measure.mac[0:14] == mac:

				sumRSSI += measure.rssi
				ctr += 1

		measures_final.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

	return measures_final


def fetchPassiveData(fpNr):

	f = open("Data/RF_bfp"+str(fpNr)+".txt",'r')

	dataset1 = []

	ap_infos = []
	scanctr = 1
	scans = []

	timestamp    = f.readline().split(',')[0]

	hours        = int(timestamp.split(':')[0])
	minutes      = int(timestamp.split(':')[1])
	seconds      = int(timestamp.split(':')[2].split('.')[0])
	milliseconds = int(timestamp.split(':')[2].split('.')[1])

	START = datetime.datetime(2015,6,20,hours,minutes,seconds,milliseconds)

	for line in f:

		# handle timestamps
		timestamp    = line.split(',')[0]

		hours        = int(timestamp.split(':')[0])
		minutes      = int(timestamp.split(':')[1])
		seconds      = int(timestamp.split(':')[2].split('.')[0])
		milliseconds = int(timestamp.split(':')[2].split('.')[1])

		NOW = datetime.datetime(2015,6,20,hours,minutes,seconds,milliseconds)

		ap_infos.append(Fingerprint.APInfo(line.split(',')[2].rstrip('\n'),int(line.split(',')[1])))

		if (NOW > (START+datetime.timedelta(seconds=1))):

			START = NOW
			scans.append(Fingerprint.Scan(scanctr,ap_infos))

			ap_infos = []
			scanctr += 1

	# Calculate Dataset 1: 3 Seconds tcpump - dynamic user (Take 1 Second from first Capture of each Channel)

	d1_macs = []
	d1_aplist_raw = []
	d1_aplist_fin = []

	# We swapped Channels every 2 Seconds

	for ap_info in scans[0].ap_infos: # Channel 1

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for ap_info in scans[3].ap_infos: # Channel 6

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for ap_info in scans[5].ap_infos: # Channel 11

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for mac in d1_macs:

		sumRSSI = 0
		ctr = 0

		for ap_info in d1_aplist_raw:

			if ap_info.mac == mac:

				sumRSSI += ap_info.rssi
				ctr += 1

		d1_aplist_fin.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

	dataset1.append(Fingerprint.Scan(scans[0].number,d1_aplist_fin))

	return dataset1[0].ap_infos


def showResults():
	""" show the Results of Initial Point, Active Scan and Passive Scan """

	pList = [[0 for x in range(3)] for x in range(len(fps))]

	distancesActive  = []
	distancesPassive = []

	passiveStdDev = 0

	for i in range(len(fps)):

		ddrActive = DistanceReasoner.Deterministic('0',8)
		ddrPassive = DistanceReasoner.Deterministic('0',8)

		activeScan = fetchActiveData(i+1)
		passiveScan = fetchPassiveData(i+1)

		activePt = ddrActive.getApproximatePosition(activeScan)
		passivePt = ddrPassive.getApproximatePosition(passiveScan)

		pList[i][0] = Fingerprint.Point(fps[i][0].x,fps[i][0].y)
		pList[i][1] = Fingerprint.Point(activePt.x,activePt.y)
		pList[i][2] = Fingerprint.Point(passivePt.x,passivePt.y)

		activeDist = math.sqrt(math.pow(activePt.x-fps[i][0].x,2)+math.pow(activePt.y-fps[i][0].y,2))
		passiveDist = math.sqrt(math.pow(passivePt.x-fps[i][0].x,2)+math.pow(passivePt.y-fps[i][0].y,2))

		distancesActive.append(activeDist)
		distancesPassive.append(passiveDist)

	active_s2  = 0
	passive_s2 = 0

	for i in range(len(fps)):

		active_s2 += math.pow(distancesActive[i]-(sum(distancesActive)/len(fps)),2)
		passive_s2 += math.pow(distancesPassive[i]-(sum(distancesPassive)/len(fps)),2)

	print "Active Scans"
	print "MeanPosError:", ((sum(distancesActive)/len(fps))/13.0)
	print "MaxPosError:", (max(distancesActive)/13.0)
	print "StdDev:", (math.sqrt(active_s2/len(fps))/13.0)
	print "\nPassive Scans"
	print "MeanPosError:", ((sum(distancesPassive)/len(fps))/13.0)
	print "MaxPosError:", (max(distancesPassive)/13.0)
	print "StdDev:", (math.sqrt(passive_s2/len(fps))/13.0)

	myMap = Map.Instance()

	if sys.argv[1] == '1':

		myMap.test1(pList,int(sys.argv[2]))

	else:

		myMap.test2(pList,int(sys.argv[2]))

	myMap.show()


def main():

	showResults()


# Run program
main()
