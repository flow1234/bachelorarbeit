from matplotlib import pyplot as plt
from matplotlib import patches as mpatches
import cv2
import math

class Instance(object):

	def __init__(self):

		# Constants
		self.INITIAL_HEIGHT = 439
		self.INITIAL_WIDTH  = 704

		# load Image and get Size		
		self.img = self.loadIMG()
		self.height = self.img.shape[0]
		self.width = self.img.shape[1]
	
		self.iPt = None
		self.aPt = None
		self.bPt = None

		# plot Image
		plt.imshow(self.img, cmap = 'gray', interpolation = 'bicubic')
		plt.xticks([]), plt.yticks([])

	def loadIMG(self): return cv2.imread('Map/plan.jpg',0)

	def scaleX(self, x): return x * self.width / self.INITIAL_WIDTH

	def scaleY(self, y): return y * self.height / self.INITIAL_HEIGHT

	def test1(self, pointsList,mode):

		for i in range(len(pointsList)):

			rA = 2*(math.sqrt(math.pow(pointsList[i][0].x-pointsList[i][1].x,2)+math.pow(pointsList[i][0].y-pointsList[i][1].y,2)))
			rP = 2*(math.sqrt(math.pow(pointsList[i][0].x-pointsList[i][2].x,2)+math.pow(pointsList[i][0].y-pointsList[i][2].y,2)))
			
			if mode == 1:
				circle1=plt.Circle((self.scaleX(pointsList[i][0].x), self.scaleY(pointsList[i][0].y)), rA,color='r',linewidth=2.,alpha=.5)
				circle2=plt.Circle((self.scaleX(pointsList[i][0].x), self.scaleY(pointsList[i][0].y)), rA,color='k',fill=False,linewidth=2.)

			else:
				circle1=plt.Circle((self.scaleX(pointsList[i][0].x), self.scaleY(pointsList[i][0].y)), rP,color='g',linewidth=2.,alpha=.7)
				circle2=plt.Circle((self.scaleX(pointsList[i][0].x), self.scaleY(pointsList[i][0].y)), rP,color='k',fill=False,linewidth=2.)

			plt.gca().add_artist(circle1)
			plt.gca().add_artist(circle2)

		for i in range(len(pointsList)):

			circle3=plt.Circle((self.scaleX(pointsList[i][0].x), self.scaleY(pointsList[i][0].y)),5,color='k')
			plt.gca().add_artist(circle3)



	def test2(self,pointsList,mode):

		for i in range(len(pointsList)):

			iP = pointsList[i][0]
			aP = pointsList[i][1]
			pP = pointsList[i][2]

			plt.scatter(self.scaleX(iP.x),self.scaleY(iP.y),color='b',s=50)

			if mode == 1:

				plt.plot([self.scaleX(iP.x),self.scaleX(aP.x)],[self.scaleY(iP.y),self.scaleY(aP.y)],linewidth=3.,color='k')
				plt.scatter(self.scaleX(aP.x),self.scaleY(aP.y),color='r',s=50)

			else:

				plt.plot([self.scaleX(iP.x),self.scaleY(pP.x)],[self.scaleY(iP.y),self.scaleY(pP.y)],linewidth=3.,color='k')
				plt.scatter(self.scaleX(pP.x),self.scaleY(pP.y),color='g',s=50)



	def show(self): 

		plt.show()
