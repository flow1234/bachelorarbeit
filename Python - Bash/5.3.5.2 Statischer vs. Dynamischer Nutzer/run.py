from matplotlib import pyplot as plt
from matplotlib import dates as dt
import datetime
import sys
import math

import DistanceReasoner
import Fingerprint


# positions (initial Point and direction)
fps = []
fps.append((Fingerprint.Point(358,93),'3'))  # 1
fps.append((Fingerprint.Point(359,322),'4')) # 2
fps.append((Fingerprint.Point(299,125),'2')) # 3
fps.append((Fingerprint.Point(359,202),'2')) # 4
fps.append((Fingerprint.Point(406,36),'1'))  # 5
fps.append((Fingerprint.Point(326,124),'4')) # 6
fps.append((Fingerprint.Point(405,381),'2')) # 7
fps.append((Fingerprint.Point(319,295),'1')) # 8
fps.append((Fingerprint.Point(323,249),'2')) # 9
fps.append((Fingerprint.Point(417,190),'2')) # 10
fps.append((Fingerprint.Point(398,146),'2')) # 11
fps.append((Fingerprint.Point(354,406),'1')) # 12
fps.append((Fingerprint.Point(363,264),'4')) # 13
fps.append((Fingerprint.Point(292,32),'1'))  # 14
fps.append((Fingerprint.Point(312,399),'2')) # 15
fps.append((Fingerprint.Point(329,380),'3')) # 16
fps.append((Fingerprint.Point(302,374),'1')) # 17
fps.append((Fingerprint.Point(326,195),'3')) # 18
fps.append((Fingerprint.Point(591,31),'2'))  # 19

maximum_k = 15
scaleMeter = 13.0

dataset1 = [] # Represents Dynamic User
dataset2 = [] # Represents Static User

resultsDynamic = [[0 for x in range(4)] for x in range(maximum_k)]
resultsStatic = [[0 for x in range(4)] for x in range(maximum_k)]


def fetchData(fpNr):
	
	f = open("Data/RF_bfp"+str(fpNr)+".txt",'r')

	del dataset1[:]
	del dataset2[:]

	ap_infos = []
	scanctr = 1
	scans = []

	timestamp    = f.readline().split(',')[0]

	hours        = int(timestamp.split(':')[0])
	minutes      = int(timestamp.split(':')[1])
	seconds      = int(timestamp.split(':')[2].split('.')[0])
	milliseconds = int(timestamp.split(':')[2].split('.')[1])

	START = datetime.datetime(2015,6,20,hours,minutes,seconds,milliseconds)

	for line in f:

		# handle timestamps
		timestamp    = line.split(',')[0]

		hours        = int(timestamp.split(':')[0])
		minutes      = int(timestamp.split(':')[1])
		seconds      = int(timestamp.split(':')[2].split('.')[0])
		milliseconds = int(timestamp.split(':')[2].split('.')[1])

		NOW = datetime.datetime(2015,6,20,hours,minutes,seconds,milliseconds)

		ap_infos.append(Fingerprint.APInfo(line.split(',')[2].rstrip('\n'),int(line.split(',')[1])))

		if (NOW > (START+datetime.timedelta(seconds=1))):

			START = NOW
			scans.append(Fingerprint.Scan(scanctr,ap_infos))

			ap_infos = []
			scanctr += 1

	# Calculate Dataset 1: 2 Seconds tcpump - dynamic user

	d1_macs = []
	d1_aplist_raw = []
	d1_aplist_fin = []

	for ap_info in scans[0].ap_infos:

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for ap_info in scans[3].ap_infos:

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for ap_info in scans[5].ap_infos:

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for mac in d1_macs:

		sumRSSI = 0
		ctr = 0

		for ap_info in d1_aplist_raw:

			if ap_info.mac == mac:

				sumRSSI += ap_info.rssi
				ctr += 1

		d1_aplist_fin.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

	dataset1.append(Fingerprint.Scan(scans[0].number,d1_aplist_fin))
	

	# Calculate Dataset 2: 20 Seconds tcpump - static user
	d2_macs       = []
	d2_aplist_raw = []
	d2_aplist_fin = []

	for scan in scans:

		for ap_info in scan.ap_infos:

			if ap_info.mac[0:14] not in d2_macs:

				d2_macs.append(ap_info.mac[0:14])

			d2_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for mac in d2_macs:

		sumRSSI = 0
		ctr = 0

		for ap_info in d2_aplist_raw:

			if ap_info.mac == mac:

				sumRSSI += ap_info.rssi
				ctr += 1

		d2_aplist_fin.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

	dataset2.append(Fingerprint.Scan(scan.number,d2_aplist_fin))


def getValues():
	""" Calculate Mean/Max Positioning Error and Standard Deviation for different Values of K """

	distancesDynamic = [[0 for x in range(len(fps))] for x in range(maximum_k)] 
	distancesStatic  = [[0 for x in range(len(fps))] for x in range(maximum_k)]

	# for every kValue < 14
	for i in range(maximum_k):

		# Initialize a Distance Reasoner based on kValue
		ddr = DistanceReasoner.Deterministic('0',i+1)

		# for every Point
		for j in range(len(fps)):

			# Read the .txt-File
			fetchData(j+1)

			# Initialize a Distance Reasoner based on kValue
			#ddr = DistanceReasoner.Deterministic(fps[j][1],i+1)

			# Get the Approximate Position
			dynamicPt = ddr.getApproximatePosition(dataset1[0].ap_infos)
			staticPt  = ddr.getApproximatePosition(dataset2[0].ap_infos)

			# Calculate Euclidean Distances
			distancesDynamic[i][j] = math.sqrt(math.pow(fps[j][0].x-dynamicPt.x,2)+math.pow(fps[j][0].y-dynamicPt.y,2))
			distancesStatic[i][j] = math.sqrt(math.pow(fps[j][0].x-staticPt.x,2)+math.pow(fps[j][0].y-staticPt.y,2))


	# Calculate the desired Values
	for i in range(maximum_k):

		# Average Positioning Error
		meanPosError_Dynamic = sum(distancesDynamic[i])/len(distancesDynamic[i])
		meanPosError_Static = sum(distancesStatic[i])/len(distancesStatic[i])

		# Maximum Positioning Error
		maxPosError_Dynamic = max(distancesDynamic[i])
		maxPosError_Static = max(distancesStatic[i])

		# Minimum Positioning Error
		minPosError_Dynamic = min(distancesDynamic[i])
		minPosError_Static = min(distancesStatic[i])

		# Standard Deviation
		s2_Dynamic = 0
		s2_Static = 0

		for j in range(len(distancesDynamic[i])):

			s2_Dynamic += math.pow(distancesDynamic[i][j]-meanPosError_Dynamic,2)
			s2_Static += math.pow(distancesStatic[i][j]-meanPosError_Static,2)

		standardDeviation_Dynamic = math.sqrt(s2_Dynamic/len(fps))
		standardDeviation_Static = math.sqrt(s2_Static/len(fps))

		# Store Results
		resultsDynamic[i][0] = meanPosError_Dynamic / scaleMeter
		resultsDynamic[i][1] = maxPosError_Dynamic / scaleMeter
		resultsDynamic[i][2] = minPosError_Dynamic / scaleMeter
		resultsDynamic[i][3] = standardDeviation_Dynamic / scaleMeter

		resultsStatic[i][0] = meanPosError_Static / scaleMeter
		resultsStatic[i][1] = maxPosError_Static / scaleMeter
		resultsStatic[i][2] = minPosError_Static / scaleMeter
		resultsStatic[i][3] = standardDeviation_Static / scaleMeter

def plotResults(inputmode):

	kValues = []
	mode = inputmode

	for i in range(maximum_k):

		kValues.append((i+1))		

	meanPosErrors_Dynamic = []
	meanPosErrors_Static = []

	maxPosErrors_Dynamic = []
	maxPosErrors_Static = []

	minPosErrors_Dynamic = []
	minPosErrors_Static = []

	stdDev_Dynamic = []
	stdDev_Static = []
	
	for i in range(len(resultsDynamic)):

		meanPosErrors_Dynamic.append(resultsDynamic[i][0])
		meanPosErrors_Static.append(resultsStatic[i][0])

		maxPosErrors_Dynamic.append(resultsDynamic[i][1])
		maxPosErrors_Static.append(resultsStatic[i][1])

		minPosErrors_Dynamic.append(resultsDynamic[i][2])
		minPosErrors_Static.append(resultsStatic[i][2])

		stdDev_Dynamic.append(resultsDynamic[i][3])
		stdDev_Static.append(resultsStatic[i][3])

	for i in range(maximum_k):

		print "K=%d" % (i+1)
		print "Dynamisch : min=%.2f m, mean=%.2f m, max=%.2f m, stdDev=%.2f m" % (minPosErrors_Dynamic[i],meanPosErrors_Dynamic[i],maxPosErrors_Dynamic[i],stdDev_Dynamic[i])
		print "Statisch  : min=%.2f m, mean=%.2f m, max=%.2f m, stdDev=%.2f m" % (minPosErrors_Static[i],meanPosErrors_Static[i],maxPosErrors_Static[i],stdDev_Static[i])


	plt.xlabel('k')
	plt.ylabel('Positionierungssfehler (in Meter)')
	plt.xlim([0,16])
	plt.grid()

	if sys.argv[1] == '1':

		plt.ylim([0,16.0])

		if sys.argv[2] == '1':
			
			plt.plot(kValues, meanPosErrors_Dynamic, label="Durchschnitt", linewidth=3.,color='g')
			plt.plot(kValues, maxPosErrors_Dynamic, label="Minimum/Maximum", linewidth=3.,color='k', linestyle='dashed')
			plt.plot(kValues, minPosErrors_Dynamic, linewidth=3.,color='k', linestyle='dashed')
			plt.legend()
			plt.savefig('Dynamic-Pos.pdf', format='pdf', dpi=1200)

		elif sys.argv[2] == '2':

			plt.plot(kValues, meanPosErrors_Static, label="Durchschnitt", linewidth=3.,color='g')
			plt.plot(kValues, maxPosErrors_Static, label="Minimum/Maximum", linewidth=3.,color='k', linestyle='dashed')
			plt.plot(kValues, minPosErrors_Static, linewidth=3.,color='k', linestyle='dashed')
			plt.legend()
			plt.savefig('Static-Pos.pdf', format='pdf', dpi=1200)

	elif sys.argv[1] =='2':

		plt.ylim([0.8,3.7])

		if sys.argv[2] =='1':

			plt.plot(kValues, stdDev_Dynamic, linewidth=3.,color='g')
			plt.ylabel('Standardabweichung (in Meter)')
			plt.savefig('Dynamic-stdDev.pdf', format='pdf', dpi=1200)
	
		elif sys.argv[2] == '2':

			plt.plot(kValues, stdDev_Static, linewidth=3.,color='g')
			plt.ylabel('Standardabweichung (in Meter)')
			plt.savefig('Static-stdDev.pdf', format='pdf', dpi=1200)
	
	plt.show()

getValues()
plotResults(sys.argv[1])
