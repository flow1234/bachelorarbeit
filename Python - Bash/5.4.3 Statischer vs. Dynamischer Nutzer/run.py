import naiveBayes
import Fingerprint
import datetime

# positions (initial Point and direction)
fps = []
fps.append(("hall",'3'))  # 1 - hall
fps.append(("corL2",'4')) # 2 - corL2
fps.append(("g002",'2'))  # 3 - G002
fps.append(("corL1",'2')) # 4 - corL1
fps.append(("corU2",'1')) # 5 - corU2
fps.append(("g002",'4'))  # 6 - G002
fps.append(("g009",'2'))  # 7 - G009
fps.append(("g008",'1'))  # 8 - G008
fps.append(("g006",'2'))  # 9 - G006
fps.append(("g003",'2'))  # 10 - G003
fps.append(("g001",'2'))  # 11 - G001 
fps.append(("corL2",'1')) # 12 - corL2
fps.append(("corL1",'4')) # 13 - corL1
fps.append(("corU2",'1')) # 14 - corU2
fps.append(("g010",'2'))  # 15 - G010
fps.append(("g010",'3'))  # 16 - G010 
fps.append(("g010",'1'))  # 17 - G010
fps.append(("g004",'3'))  # 18 - G004
fps.append(("corU3",'2')) # 19 - corU3

dataset1 = [] # dynamic User
dataset2 = [] # static User

def fetchData(fpNr):
	
	f = open("Data/RF_bfp"+str(fpNr)+".txt",'r')

	del dataset1[:]
	del dataset2[:]

	ap_infos = []
	scanctr = 1
	scans = []

	timestamp    = f.readline().split(',')[0]

	hours        = int(timestamp.split(':')[0])
	minutes      = int(timestamp.split(':')[1])
	seconds      = int(timestamp.split(':')[2].split('.')[0])
	milliseconds = int(timestamp.split(':')[2].split('.')[1])

	START = datetime.datetime(2015,6,20,hours,minutes,seconds,milliseconds)

	for line in f:

		# handle timestamps
		timestamp    = line.split(',')[0]

		hours        = int(timestamp.split(':')[0])
		minutes      = int(timestamp.split(':')[1])
		seconds      = int(timestamp.split(':')[2].split('.')[0])
		milliseconds = int(timestamp.split(':')[2].split('.')[1])

		NOW = datetime.datetime(2015,6,20,hours,minutes,seconds,milliseconds)

		ap_infos.append(Fingerprint.APInfo(line.split(',')[2].rstrip('\n'),int(line.split(',')[1])))

		if (NOW > (START+datetime.timedelta(seconds=1))):

			START = NOW
			scans.append(Fingerprint.Scan(scanctr,ap_infos))

			ap_infos = []
			scanctr += 1

	# Calculate Dataset 1: 2 Seconds tcpump - dynamic user

	d1_macs = []
	d1_aplist_raw = []
	d1_aplist_fin = []

	for ap_info in scans[0].ap_infos:

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for ap_info in scans[3].ap_infos:

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for ap_info in scans[5].ap_infos:

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for mac in d1_macs:

		sumRSSI = 0
		ctr = 0

		for ap_info in d1_aplist_raw:

			if ap_info.mac == mac:

				sumRSSI += ap_info.rssi
				ctr += 1

		d1_aplist_fin.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

	dataset1.append(Fingerprint.Scan(scans[0].number,d1_aplist_fin))
	

	# Calculate Dataset 2: 20 Seconds tcpump - static user
	d2_macs       = []
	d2_aplist_raw = []
	d2_aplist_fin = []

	for scan in scans:

		for ap_info in scan.ap_infos:

			if ap_info.mac[0:14] not in d2_macs:

				d2_macs.append(ap_info.mac[0:14])

			d2_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for mac in d2_macs:

		sumRSSI = 0
		ctr = 0

		for ap_info in d2_aplist_raw:

			if ap_info.mac == mac:

				sumRSSI += ap_info.rssi
				ctr += 1

		d2_aplist_fin.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

	dataset2.append(Fingerprint.Scan(scan.number,d2_aplist_fin))


def main():
	""" Main Function """

	# for every Point
	for j in range(len(fps)):	
		fetchData(j+1)

		nBayes = naiveBayes.Filter('0')

		print "\nFingerprint %d" % (j+1)
		print "Expected Room: %s" % (fps[j][0])
		print "Dynamic:", nBayes.getRoom(dataset1[0].ap_infos)
		print "Static:", nBayes.getRoom(dataset2[0].ap_infos)

main()
