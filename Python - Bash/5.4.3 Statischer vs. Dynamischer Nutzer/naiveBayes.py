import sqlite3 as lite
from operator import itemgetter as ig
import Fingerprint
import sys

class Filter(object):

	def __init__(self, direction):
		""" Main Constructor """

		self.direction = direction
		self.all_Measurements = 0
		self.fingerprints = []
		self.rooms = []

		self.loadFingerprintData()
		self.calcApriori()


	def calcApriori(self):
		""" Calculate the the Room You're most likely in """

		# Step 1: Get every unique Room

		uniqueRooms = [] # List of unique Room-Names from Database
		rooms = []       # List of Room-Objects (name,prob,fps)

		for fp in self.fingerprints:

			if fp.room not in uniqueRooms:

				uniqueRooms.append(fp.room)

		for room in uniqueRooms:

			self.rooms.append(Room(room))


		# Step 2: Get Number of Measurements in each Room + Add corresponding Fingerprints to List

		for room in self.rooms:

			for fp in self.fingerprints:

				if fp.room == room.name:

					room.fps.append(fp)

					for wifi_scan in fp.wifi_scans:

						room.measuresCount += len(wifi_scan.ap_infos)


		# Step 3: Get Number of ALL Measurements

		for room in self.rooms:

			self.all_Measurements += room.measuresCount


		# Step 4: Get Probability for being inside a certain Room
		#for room in self.rooms:

		#	tempValue = float(room.prob) / float(self.all_Measurements)
		#	room.prob = tempValue


	def getRoom(self,measures):
		"""P(R|M1_M2_.._Mn) - Probability for being inside Room r based on Measure-Set """

		# Step 1: Calculate Probabilities for being inside each Room

		# For each Room
		# -> For each Measure (MAC|RSSI)
		#    -> Search every FP inside that room, every Scan for each FP, every Measure for each Scan
		#	 	-> If Match
		#		   -> Count
		#    -> Calculate Count/AllMeasuresInRoom
		# -> Multiply each Result (count/meas_1 * count/meas_2 * etc.)
		# -> Multiply with (AllMeasuresInRoom/AllMeasuresInDatabase)
		#
		# Compare Results and return highest probability

		# LAPLACE-SMOOTHING
		#
		#  Count        Count + k
		# ------- => ----------------
		#    N        N + k * |macs|
		#
		#
		#     k  ~ LaplaceSmoother
		# |macs| ~ Number different Macs in measures

		results = []
		macs = []

		for measure in measures:

			if measure.mac[0:14] not in macs:

				macs.append(measure.mac[0:14])

		# Laplace-Smoother
		LP_k = 1
		LP_M = len(macs)

		for room in self.rooms:

			result = 1    # ( P(M_1|R_1) * P(M_2|R_1) * ... * P(M_n|R_1) )

			for measure in measures:

				measCount = 0

				for fp in room.fps:

					for wifi_scan in fp.wifi_scans:

						for ap_info in wifi_scan.ap_infos:

							if ap_info.mac[0:14] == measure.mac[0:14] and ap_info.rssi == measure.rssi:

								measCount += 1

				result *= (float(measCount+LP_k)/float(room.measuresCount+(LP_k*LP_M)))

			result *= (float(room.measuresCount+LP_k)/float(self.all_Measurements+(LP_k*LP_M)))

			results.append((room.name,result))

		results.sort(key=ig(1),reverse=True)

		#for r in results:

		#	print "%.20f - Room %s" % (r[1],r[0])

		return results[0][0]



	def loadFingerprintData(self):
		""" load Fingerprints from Database """

		try:

			con = lite.connect('Database/DBFinal.sqlite')

			cur = con.cursor()

			if self.direction == '0':

				cur.execute("SELECT * FROM probabilistic_fingerprints")

			else:

				cur.execute("SELECT * FROM probabilistic_fingerprints WHERE direction=?",self.direction)

			data = cur.fetchall()

			for entry in data:

				_id = entry[0]
				x   = entry[1]
				y   = entry[2]
				d   = entry[3]
				s   = entry[4]
				r   = entry[5]

				self.fingerprints.append(Fingerprint.Data(_id,x,y,d,s,r))

		except lite.Error, e:

			print "Error %s:" % e.args[0]
			sys.exit(1)

		finally:
	
			if con:

				con.close()

class Room:
	""" Room Object """
	
	def __init__(self,name):
		self.name          = name
		self.measuresCount = 0
		self.fps           = []
