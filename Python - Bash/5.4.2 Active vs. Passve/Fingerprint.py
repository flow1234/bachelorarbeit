import json

class Data:
	""" Fingerprint Object """
	def __init__(self,_id,x,y,direction,wifi_scans,room):
		self._id = _id
		self.x = x
		self.y = y
		self.direction = direction
		self.wifi_scans = convertJSON(wifi_scans)
		self.room = room	

class Scan:
	""" Scan Object """
	def __init__(self,number,ap_infos):
		self.number   = number
		self.ap_infos = ap_infos

class APInfo:
	""" APInfo Object """
	def __init__(self,mac,rssi):
		self.mac  = mac
		self.rssi = rssi

class Point:
	""" 2D-Point """
	def __init__(self,x,y):
		self.x = x
		self.y = y	


def convertJSON(string):
	""" Convert JSON-String from SQLite to Python List """
	wifi_scans = []
#	ap_infos   = []

	result = json.loads(string)

	for i in range(len(result)):

		ap_infos   = []

		for j in range(len(result[i]['ap_list'])):

			ap_infos.append(APInfo(result[i]['ap_list'][j]['mac'],result[i]['ap_list'][j]['rssi']))

		wifi_scans.append(Scan((i+1),ap_infos))

	return wifi_scans
