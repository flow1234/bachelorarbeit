import naiveBayes
import Fingerprint
import datetime

# positions (initial Point and direction)
fps = []
fps.append(("hall",'3'))  # 1 - hall
fps.append(("corL2",'4')) # 2 - corL2
fps.append(("g002",'2'))  # 3 - G002
fps.append(("corL1",'2')) # 4 - corL1
fps.append(("corU2",'1')) # 5 - corU2
fps.append(("g002",'4'))  # 6 - G002
fps.append(("g009",'2'))  # 7 - G009
fps.append(("g008",'1'))  # 8 - G008
fps.append(("g006",'2'))  # 9 - G006
fps.append(("g003",'2'))  # 10 - G003
fps.append(("g001",'2'))  # 11 - G001 
fps.append(("corL2",'1')) # 12 - corL2
fps.append(("corL1",'4')) # 13 - corL1
fps.append(("corU2",'1')) # 14 - corU2
fps.append(("g010",'2'))  # 15 - G010
fps.append(("g010",'3'))  # 16 - G010 
fps.append(("g010",'1'))  # 17 - G010
fps.append(("g004",'3'))  # 18 - G004
fps.append(("corU3",'2')) # 19 - corU3

def fetchActiveData(fpNr):
	""" get all scans from .txt-File """

	f = open("Data/RF_afp"+str(fpNr)+".txt",'r')

	measures_raw = []
	measures_final = []
	macs = []

	for line in f:

		measures_raw.append(Fingerprint.APInfo(line.split(',')[2].rstrip('\n'),int(line.split(',')[1])))

	for measure in measures_raw:

		if measure.mac[0:14] not in macs:

			macs.append(measure.mac[0:14])

	for mac in macs:

		sumRSSI = 0
		ctr = 0

		for measure in measures_raw:

			if measure.mac[0:14] == mac:

				sumRSSI += measure.rssi
				ctr += 1

		measures_final.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

	return measures_final


def fetchPassiveData(fpNr):

	f = open("Data/RF_bfp"+str(fpNr)+".txt",'r')

	dataset1 = []

	ap_infos = []
	scanctr = 1
	scans = []

	timestamp    = f.readline().split(',')[0]

	hours        = int(timestamp.split(':')[0])
	minutes      = int(timestamp.split(':')[1])
	seconds      = int(timestamp.split(':')[2].split('.')[0])
	milliseconds = int(timestamp.split(':')[2].split('.')[1])

	START = datetime.datetime(2015,6,20,hours,minutes,seconds,milliseconds)

	for line in f:

		# handle timestamps
		timestamp    = line.split(',')[0]

		hours        = int(timestamp.split(':')[0])
		minutes      = int(timestamp.split(':')[1])
		seconds      = int(timestamp.split(':')[2].split('.')[0])
		milliseconds = int(timestamp.split(':')[2].split('.')[1])

		NOW = datetime.datetime(2015,6,20,hours,minutes,seconds,milliseconds)

		ap_infos.append(Fingerprint.APInfo(line.split(',')[2].rstrip('\n'),int(line.split(',')[1])))

		if (NOW > (START+datetime.timedelta(seconds=1))):

			START = NOW
			scans.append(Fingerprint.Scan(scanctr,ap_infos))

			ap_infos = []
			scanctr += 1

	# Calculate Dataset 1: 3 Seconds tcpump - dynamic user (Take 1 Second from first Capture of each Channel)

	d1_macs = []
	d1_aplist_raw = []
	d1_aplist_fin = []

	# We swapped Channels every 2 Seconds

	for ap_info in scans[0].ap_infos: # Channel 1

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for ap_info in scans[3].ap_infos: # Channel 6

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for ap_info in scans[5].ap_infos: # Channel 11

		if ap_info.mac[0:14] not in d1_macs:

			d1_macs.append(ap_info.mac[0:14])

		d1_aplist_raw.append(Fingerprint.APInfo(ap_info.mac[0:14],ap_info.rssi))

	for mac in d1_macs:

		sumRSSI = 0
		ctr = 0

		for ap_info in d1_aplist_raw:

			if ap_info.mac == mac:

				sumRSSI += ap_info.rssi
				ctr += 1

		d1_aplist_fin.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

	dataset1.append(Fingerprint.Scan(scans[0].number,d1_aplist_fin))

	return dataset1[0].ap_infos


def main():
	""" Main Function """

	# for every Point
	for j in range(len(fps)):	
		activeScan  = fetchActiveData(j+1)
		passiveScan = fetchPassiveData(j+1)

		nBayes = naiveBayes.Filter(fps[j][1])

		print "\nFingerprint %d" % (j+1)
		print "Expected Room: %s" % (fps[j][0])
		print "Active:", nBayes.getRoom(activeScan)
		print "Passive:", nBayes.getRoom(passiveScan)

main()
