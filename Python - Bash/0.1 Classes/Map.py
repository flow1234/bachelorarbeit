from matplotlib import pyplot as plt
from matplotlib import patches as mpatches
import cv2

class Instance(object):

	def __init__(self):

		# Constants
		self.INITIAL_HEIGHT = 439
		self.INITIAL_WIDTH  = 704

		# load Image and get Size		
		self.img = self.loadIMG()
		self.height = self.img.shape[0]
		self.width = self.img.shape[1]
	
		self.iPt = None
		self.aPt = None
		self.bPt = None

		# plot Image
		plt.imshow(self.img, cmap = 'gray', interpolation = 'bicubic')
		plt.xticks([]), plt.yticks([])

	def loadIMG(self): return cv2.imread('Map/plan.jpg',0)

	def scaleX(self, x): return x * self.width / self.INITIAL_WIDTH

	def scaleY(self, y): return y * self.height / self.INITIAL_HEIGHT

	def setAPoint(self, x, y):

		self.aPt = plt.scatter(self.scaleX(x), self.scaleY(y), color='m', s=25)

	def setBPoint(self, x, y):

		self.bPt = plt.scatter(self.scaleX(x), self.scaleY(y), color='b', s=25)

	def setIPoint(self, x, y):

		self.iPt = plt.scatter(self.scaleX(x), self.scaleY(y), color='r', s=25)

	def showLegend(self,kValue,direction):

		plt.legend((self.iPt,self.aPt,self.bPt),
                   ('Initial Point','Active Scan','Passive Scan'),
                   scatterpoints=1,
				   loc='lower left',
				   fontsize=12)

		plt.text(-35,670,'kValue = %d\ndirection = %s' % (kValue,direction), bbox={'facecolor':'white', 'alpha':0.9, 'pad':10})

	def show(self,kValue,direction): 

		self.showLegend(kValue,direction)
		plt.show()
