import sqlite3 as lite
import Fingerprint
import sys

dbFps = []
refactoredFps = []

def loadPoints():

	try:

		con = lite.connect('DBFinal.sqlite')

		cur = con.cursor()

		cur.execute("SELECT * FROM probabilistic_fingerprints")

		data = cur.fetchall()

		for entry in data:

			_id = entry[0]
			x = entry[1]
			y = entry[2]
			d = entry[3]
			s = entry[4]
			r = entry[5]
			dbFps.append(Fingerprint.ProbData(_id,x,y,d,s,"temp"))

	except lite.Error, e:

		print "Error %s:" % e.args[0]
		sys.exit(1)

	finally:

		if con:

			con.close()


def getMedian():

	for fp in dbFps:

		finalScans = []

		for wifi_scan in fp.wifi_scans:

			tempList = []
			finalList = []
			macs = []

			for ap_info in wifi_scan.ap_infos:

				tempList.append(ap_info)

				if ap_info.mac[0:14] not in macs:

					macs.append(ap_info.mac[0:14])

			for mac in macs:

				sumRSSI = 0
				ctr = 0

				for measure in tempList:

					if measure.mac[0:14] == mac:

						sumRSSI += measure.rssi
						ctr += 1

				finalRSSI = (sumRSSI/ctr)
				finalList.append(Fingerprint.APInfo(mac,finalRSSI))

			finalScans.append(Fingerprint.Scan(wifi_scan.number,finalList))

		refactoredFps.append(Fingerprint.RawData(fp._id,fp.x,fp.y,fp.direction,finalScans,fp.room))


def apInfosToJSON(scans):

	test = "["

	for scan in scans:

		if scan.number > 1:

			test += ","

		test += "{\"ap_list\":["

		for i in range(len(scan.ap_infos)):

			test += "{\"mac\":\"" + scan.ap_infos[i].mac + "\"," + "\"rssi\":" + str(scan.ap_infos[i].rssi) + "}"

			if i == (len(scan.ap_infos)-1):

				test += "],\"scan_id\":"
				test += str(scan.number)
				test += "}"

			else:

				test += ","

	test += "]"

	return test


def writeChanges():

	try:

		con = lite.connect('DBFinal.sqlite')

		cur = con.cursor()

		for fp in refactoredFps:

			cur.execute('UPDATE probabilistic_fingerprints SET wifi_scans=? WHERE _id=?', (apInfosToJSON(fp.wifi_scans), fp._id))

		con.commit()


	except lite.Error, e:

		print "Error %s:" % e.args[0]
		sys.exit(1)

	finally:

		if con:

			con.close()


def main():

	loadPoints() 	# load Points from Database
	getMedian() 	# temporarily get median_values
	writeChanges()  # write changes
#	for apinfo in refactoredFps[0].wifi_scans.ap_infos:

	print apInfosToJSON(refactoredFps[0].wifi_scans)

	#print "Testing"
	#print "Old Data for FP %d" % dbFps[4]
"""
	print apInfosToJSON(refactoredFps[0].wifi_scans)
	print "Old length", len(dbFps[0].wifi_scans[0].ap_infos)
	print "New length", len(refactoredFps[0].wifi_scans[0].ap_infos)

	print "OLD DATA"

	for wifi_scan in dbFps[0].wifi_scans:

		print "NUMBER %d" % wifi_scan.number
	
		for ap_info in wifi_scan.ap_infos:

			print "MAC=%s, RSSI=%d" % (ap_info.mac, ap_info.rssi)

	print "NEW DATA"

	for wifi_scan in refactoredFps[0].wifi_scans:

		print "NUMBER %d" % wifi_scan.number

		for ap_info in wifi_scan.ap_infos:

			print "MAC=%s, RSSI=%d" % (ap_info.mac, ap_info.rssi)

"""
main()

"""
[{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-72},{"mac":"00:03:52:ab:82:c6","rssi":-73},{"mac":"00:03:52:ab:82:c2","rssi":-73},{"mac":"00:03:52:ab:82:c3","rssi":-73},{"mac":"00:03:52:ab:82:c0","rssi":-73},{"mac":"00:03:52:ab:82:c4","rssi":-73},{"mac":"9c:1c:12:c1:01:85","rssi":-77},{"mac":"9c:1c:12:c1:01:81","rssi":-77},{"mac":"9c:1c:12:c1:01:82","rssi":-77},{"mac":"9c:1c:12:c1:01:83","rssi":-78},{"mac":"9c:1c:12:c1:01:84","rssi":-78},{"mac":"9c:1c:12:c1:01:80","rssi":-76},{"mac":"2c:41:38:ef:3b:f4","rssi":-82},{"mac":"2c:41:38:ef:3b:f0","rssi":-76},{"mac":"2c:41:38:ef:3b:f6","rssi":-81},{"mac":"c0:4a:00:05:4f:1c","rssi":-72},{"mac":"00:1b:11:f3:85:3b","rssi":-73}],"scan_id":1},

{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-80},{"mac":"00:03:52:ab:82:c6","rssi":-80},{"mac":"00:03:52:ab:82:c2","rssi":-79},{"mac":"00:03:52:ab:82:c3","rssi":-80},{"mac":"00:03:52:ab:82:c0","rssi":-81},{"mac":"00:03:52:ab:82:c4","rssi":-79},{"mac":"9c:1c:12:c1:01:82","rssi":-89},{"mac":"9c:1c:12:c1:01:80","rssi":-88},{"mac":"2c:41:38:ef:3b:f4","rssi":-82},{"mac":"2c:41:38:ef:3b:f6","rssi":-81},{"mac":"c0:4a:00:05:4f:1c","rssi":-78},{"mac":"00:1b:11:f3:85:3b","rssi":-74}],"scan_id":2},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-68},{"mac":"00:03:52:ab:82:c6","rssi":-68},{"mac":"00:03:52:ab:82:c2","rssi":-68},{"mac":"00:03:52:ab:82:c3","rssi":-68},{"mac":"00:03:52:ab:82:c0","rssi":-68},{"mac":"00:03:52:ab:82:c4","rssi":-68},{"mac":"9c:1c:12:c1:01:82","rssi":-84},{"mac":"9c:1c:12:c1:01:80","rssi":-84},{"mac":"c0:4a:00:05:4f:1c","rssi":-73},{"mac":"00:1b:11:f3:85:3b","rssi":-74},{"mac":"9c:1c:12:c1:01:81","rssi":-84},{"mac":"9c:1c:12:c1:01:83","rssi":-84},{"mac":"9c:1c:12:c1:01:85","rssi":-86},{"mac":"2c:41:38:ef:3b:f0","rssi":-86},{"mac":"9c:1c:12:c1:01:84","rssi":-84}],"scan_id":3},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-68},{"mac":"00:03:52:ab:82:c6","rssi":-68},{"mac":"00:03:52:ab:82:c2","rssi":-68},{"mac":"00:03:52:ab:82:c3","rssi":-91},{"mac":"00:03:52:ab:82:c0","rssi":-68},{"mac":"00:03:52:ab:82:c4","rssi":-68},{"mac":"9c:1c:12:c1:01:82","rssi":-84},{"mac":"9c:1c:12:c1:01:80","rssi":-85},{"mac":"c0:4a:00:05:4f:1c","rssi":-74},{"mac":"00:1b:11:f3:85:3b","rssi":-75},{"mac":"9c:1c:12:c1:01:81","rssi":-85},{"mac":"9c:1c:12:c1:01:83","rssi":-84},{"mac":"9c:1c:12:c1:01:85","rssi":-83},{"mac":"2c:41:38:ef:3b:f0","rssi":-86},{"mac":"9c:1c:12:c1:01:84","rssi":-85},{"mac":"2c:41:38:ef:3b:f4","rssi":-85},{"mac":"2c:41:38:ef:3b:f6","rssi":-86}],"scan_id":4},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-77},{"mac":"00:03:52:ab:82:c6","rssi":-77},{"mac":"00:03:52:ab:82:c2","rssi":-77},{"mac":"00:03:52:ab:82:c3","rssi":-77},{"mac":"00:03:52:ab:82:c0","rssi":-77},{"mac":"00:03:52:ab:82:c4","rssi":-77},{"mac":"9c:1c:12:c1:01:82","rssi":-83},{"mac":"9c:1c:12:c1:01:80","rssi":-83},{"mac":"c0:4a:00:05:4f:1c","rssi":-71},{"mac":"00:1b:11:f3:85:3b","rssi":-71},{"mac":"9c:1c:12:c1:01:81","rssi":-82},{"mac":"9c:1c:12:c1:01:83","rssi":-82},{"mac":"9c:1c:12:c1:01:85","rssi":-82},{"mac":"2c:41:38:ef:3b:f0","rssi":-85},{"mac":"9c:1c:12:c1:01:84","rssi":-82},{"mac":"2c:41:38:ef:3b:f4","rssi":-85},{"mac":"2c:41:38:ef:3b:f6","rssi":-86}],"scan_id":5},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-78},{"mac":"00:03:52:ab:82:c6","rssi":-79},{"mac":"00:03:52:ab:82:c2","rssi":-80},{"mac":"00:03:52:ab:82:c3","rssi":-79},{"mac":"00:03:52:ab:82:c0","rssi":-80},{"mac":"00:03:52:ab:82:c4","rssi":-79},{"mac":"9c:1c:12:c1:01:82","rssi":-82},{"mac":"9c:1c:12:c1:01:80","rssi":-81},{"mac":"c0:4a:00:05:4f:1c","rssi":-72},{"mac":"00:1b:11:f3:85:3b","rssi":-72},{"mac":"9c:1c:12:c1:01:81","rssi":-81},{"mac":"9c:1c:12:c1:01:83","rssi":-81},{"mac":"9c:1c:12:c1:01:85","rssi":-81},{"mac":"2c:41:38:ef:3b:f0","rssi":-85},{"mac":"9c:1c:12:c1:01:84","rssi":-82},{"mac":"2c:41:38:ef:3b:f4","rssi":-87},{"mac":"2c:41:38:ef:3b:f6","rssi":-87},{"mac":"2c:41:38:ef:3b:f1","rssi":-87}],"scan_id":6},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-79},{"mac":"00:03:52:ab:82:c6","rssi":-80},{"mac":"00:03:52:ab:82:c2","rssi":-79},{"mac":"00:03:52:ab:82:c3","rssi":-80},{"mac":"00:03:52:ab:82:c0","rssi":-80},{"mac":"00:03:52:ab:82:c4","rssi":-79},{"mac":"9c:1c:12:c1:01:82","rssi":-83},{"mac":"9c:1c:12:c1:01:80","rssi":-82},{"mac":"c0:4a:00:05:4f:1c","rssi":-73},{"mac":"00:1b:11:f3:85:3b","rssi":-70},{"mac":"9c:1c:12:c1:01:81","rssi":-84},{"mac":"9c:1c:12:c1:01:83","rssi":-83},{"mac":"9c:1c:12:c1:01:85","rssi":-84},{"mac":"9c:1c:12:c1:01:84","rssi":-83},{"mac":"2c:41:38:ef:3b:f4","rssi":-87},{"mac":"2c:41:38:ef:3b:f6","rssi":-87},{"mac":"2c:41:38:ef:3b:f1","rssi":-87}],"scan_id":7},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-76},{"mac":"00:03:52:ab:82:c6","rssi":-76},{"mac":"00:03:52:ab:82:c2","rssi":-76},{"mac":"00:03:52:ab:82:c3","rssi":-76},{"mac":"00:03:52:ab:82:c0","rssi":-76},{"mac":"00:03:52:ab:82:c4","rssi":-75},{"mac":"9c:1c:12:c1:01:82","rssi":-81},{"mac":"9c:1c:12:c1:01:80","rssi":-81},{"mac":"c0:4a:00:05:4f:1c","rssi":-71},{"mac":"00:1b:11:f3:85:3b","rssi":-71},{"mac":"9c:1c:12:c1:01:81","rssi":-82},{"mac":"9c:1c:12:c1:01:83","rssi":-81},{"mac":"9c:1c:12:c1:01:85","rssi":-81},{"mac":"9c:1c:12:c1:01:84","rssi":-81}],"scan_id":8},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-76},{"mac":"00:03:52:ab:82:c6","rssi":-76},{"mac":"00:03:52:ab:82:c2","rssi":-77},{"mac":"00:03:52:ab:82:c3","rssi":-76},{"mac":"00:03:52:ab:82:c0","rssi":-77},{"mac":"00:03:52:ab:82:c4","rssi":-76},{"mac":"9c:1c:12:c1:01:82","rssi":-81},{"mac":"9c:1c:12:c1:01:80","rssi":-82},{"mac":"c0:4a:00:05:4f:1c","rssi":-74},{"mac":"00:1b:11:f3:85:3b","rssi":-69},{"mac":"9c:1c:12:c1:01:81","rssi":-82},{"mac":"9c:1c:12:c1:01:83","rssi":-81},{"mac":"9c:1c:12:c1:01:85","rssi":-82},{"mac":"9c:1c:12:c1:01:84","rssi":-81},{"mac":"2c:41:38:ef:3b:f4","rssi":-88},{"mac":"2c:41:38:ef:3b:f0","rssi":-88},{"mac":"2c:41:38:ef:3b:f1","rssi":-87}],"scan_id":9},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-70},{"mac":"00:03:52:ab:82:c6","rssi":-70},{"mac":"00:03:52:ab:82:c2","rssi":-70},{"mac":"00:03:52:ab:82:c3","rssi":-70},{"mac":"00:03:52:ab:82:c0","rssi":-70},{"mac":"00:03:52:ab:82:c4","rssi":-70},{"mac":"9c:1c:12:c1:01:82","rssi":-85},{"mac":"9c:1c:12:c1:01:80","rssi":-83},{"mac":"c0:4a:00:05:4f:1c","rssi":-71},{"mac":"00:1b:11:f3:85:3b","rssi":-70},{"mac":"9c:1c:12:c1:01:81","rssi":-83},{"mac":"9c:1c:12:c1:01:83","rssi":-84},{"mac":"9c:1c:12:c1:01:85","rssi":-84},{"mac":"9c:1c:12:c1:01:84","rssi":-84},{"mac":"2c:41:38:ef:3b:f4","rssi":-86},{"mac":"2c:41:38:ef:3b:f0","rssi":-88},{"mac":"2c:41:38:ef:3b:f1","rssi":-87},{"mac":"2c:41:38:ef:3b:f6","rssi":-85}],"scan_id":10},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-85},{"mac":"00:03:52:ab:82:c6","rssi":-85},{"mac":"00:03:52:ab:82:c2","rssi":-84},{"mac":"00:03:52:ab:82:c3","rssi":-85},{"mac":"00:03:52:ab:82:c0","rssi":-87},{"mac":"00:03:52:ab:82:c4","rssi":-83},{"mac":"9c:1c:12:c1:01:82","rssi":-84},{"mac":"9c:1c:12:c1:01:80","rssi":-83},{"mac":"c0:4a:00:05:4f:1c","rssi":-70},{"mac":"00:1b:11:f3:85:3b","rssi":-71},{"mac":"9c:1c:12:c1:01:81","rssi":-84},{"mac":"9c:1c:12:c1:01:83","rssi":-84},{"mac":"9c:1c:12:c1:01:85","rssi":-84},{"mac":"9c:1c:12:c1:01:84","rssi":-84},{"mac":"2c:41:38:ef:3b:f4","rssi":-86},{"mac":"2c:41:38:ef:3b:f6","rssi":-85}],"scan_id":11},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-86},{"mac":"00:03:52:ab:82:c6","rssi":-86},{"mac":"00:03:52:ab:82:c2","rssi":-86},{"mac":"00:03:52:ab:82:c3","rssi":-87},{"mac":"00:03:52:ab:82:c0","rssi":-86},{"mac":"00:03:52:ab:82:c4","rssi":-86},{"mac":"9c:1c:12:c1:01:82","rssi":-84},{"mac":"9c:1c:12:c1:01:80","rssi":-83},{"mac":"c0:4a:00:05:4f:1c","rssi":-69},{"mac":"00:1b:11:f3:85:3b","rssi":-74},{"mac":"9c:1c:12:c1:01:81","rssi":-87},{"mac":"9c:1c:12:c1:01:83","rssi":-87},{"mac":"9c:1c:12:c1:01:85","rssi":-87},{"mac":"9c:1c:12:c1:01:84","rssi":-87},{"mac":"2c:41:38:ef:3b:f0","rssi":-84},{"mac":"2c:41:38:ef:3b:f1","rssi":-83}],"scan_id":12},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-87},{"mac":"00:03:52:ab:82:c6","rssi":-87},{"mac":"00:03:52:ab:82:c2","rssi":-87},{"mac":"00:03:52:ab:82:c3","rssi":-85},{"mac":"00:03:52:ab:82:c0","rssi":-88},{"mac":"00:03:52:ab:82:c4","rssi":-86},{"mac":"9c:1c:12:c1:01:82","rssi":-87},{"mac":"9c:1c:12:c1:01:80","rssi":-86},{"mac":"c0:4a:00:05:4f:1c","rssi":-67},{"mac":"00:1b:11:f3:85:3b","rssi":-74},{"mac":"9c:1c:12:c1:01:81","rssi":-86},{"mac":"9c:1c:12:c1:01:83","rssi":-86},{"mac":"9c:1c:12:c1:01:85","rssi":-87},{"mac":"9c:1c:12:c1:01:84","rssi":-86},{"mac":"2c:41:38:ef:3b:f0","rssi":-84},{"mac":"2c:41:38:ef:3b:f1","rssi":-83},{"mac":"2c:41:38:ef:3b:f6","rssi":-83},{"mac":"2c:41:38:ef:3b:f4","rssi":-85}],"scan_id":13},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-86},{"mac":"00:03:52:ab:82:c6","rssi":-86},{"mac":"00:03:52:ab:82:c2","rssi":-86},{"mac":"00:03:52:ab:82:c3","rssi":-87},{"mac":"00:03:52:ab:82:c0","rssi":-88},{"mac":"00:03:52:ab:82:c4","rssi":-87},{"mac":"9c:1c:12:c1:01:82","rssi":-87},{"mac":"9c:1c:12:c1:01:80","rssi":-87},{"mac":"c0:4a:00:05:4f:1c","rssi":-67},{"mac":"00:1b:11:f3:85:3b","rssi":-74},{"mac":"9c:1c:12:c1:01:81","rssi":-89},{"mac":"9c:1c:12:c1:01:83","rssi":-86},{"mac":"9c:1c:12:c1:01:85","rssi":-90},{"mac":"9c:1c:12:c1:01:84","rssi":-86},{"mac":"2c:41:38:ef:3b:f6","rssi":-83},{"mac":"2c:41:38:ef:3b:f4","rssi":-85}],"scan_id":14},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-86},{"mac":"00:03:52:ab:82:c6","rssi":-84},{"mac":"00:03:52:ab:82:c2","rssi":-85},{"mac":"00:03:52:ab:82:c3","rssi":-84},{"mac":"00:03:52:ab:82:c0","rssi":-84},{"mac":"00:03:52:ab:82:c4","rssi":-84},{"mac":"9c:1c:12:c1:01:82","rssi":-85},{"mac":"9c:1c:12:c1:01:80","rssi":-86},{"mac":"c0:4a:00:05:4f:1c","rssi":-67},{"mac":"00:1b:11:f3:85:3b","rssi":-71},{"mac":"9c:1c:12:c1:01:81","rssi":-84},{"mac":"9c:1c:12:c1:01:83","rssi":-85},{"mac":"9c:1c:12:c1:01:85","rssi":-85},{"mac":"9c:1c:12:c1:01:84","rssi":-84},{"mac":"2c:41:38:ef:3b:f4","rssi":-85},{"mac":"2c:41:38:ef:3b:f0","rssi":-84},{"mac":"2c:41:38:ef:3b:f1","rssi":-84}],"scan_id":15},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-78},{"mac":"00:03:52:ab:82:c6","rssi":-79},{"mac":"00:03:52:ab:82:c2","rssi":-78},{"mac":"00:03:52:ab:82:c3","rssi":-79},{"mac":"00:03:52:ab:82:c0","rssi":-79},{"mac":"00:03:52:ab:82:c4","rssi":-78},{"mac":"9c:1c:12:c1:01:82","rssi":-83},{"mac":"9c:1c:12:c1:01:80","rssi":-83},{"mac":"c0:4a:00:05:4f:1c","rssi":-68},{"mac":"00:1b:11:f3:85:3b","rssi":-74},{"mac":"9c:1c:12:c1:01:81","rssi":-83},{"mac":"9c:1c:12:c1:01:83","rssi":-82},{"mac":"9c:1c:12:c1:01:85","rssi":-83},{"mac":"9c:1c:12:c1:01:84","rssi":-83},{"mac":"2c:41:38:ef:3b:f4","rssi":-86},{"mac":"2c:41:38:ef:3b:f0","rssi":-84},{"mac":"2c:41:38:ef:3b:f1","rssi":-84},{"mac":"2c:41:38:ef:3b:f6","rssi":-88}],"scan_id":16},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-76},{"mac":"00:03:52:ab:82:c6","rssi":-76},{"mac":"00:03:52:ab:82:c2","rssi":-76},{"mac":"00:03:52:ab:82:c3","rssi":-75},{"mac":"00:03:52:ab:82:c0","rssi":-76},{"mac":"00:03:52:ab:82:c4","rssi":-75},{"mac":"9c:1c:12:c1:01:82","rssi":-87},{"mac":"9c:1c:12:c1:01:80","rssi":-87},{"mac":"c0:4a:00:05:4f:1c","rssi":-70},{"mac":"00:1b:11:f3:85:3b","rssi":-69},{"mac":"9c:1c:12:c1:01:81","rssi":-87},{"mac":"9c:1c:12:c1:01:83","rssi":-87},{"mac":"9c:1c:12:c1:01:85","rssi":-87},{"mac":"9c:1c:12:c1:01:84","rssi":-83},{"mac":"2c:41:38:ef:3b:f4","rssi":-86},{"mac":"2c:41:38:ef:3b:f6","rssi":-88}],"scan_id":17},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-75},{"mac":"00:03:52:ab:82:c6","rssi":-75},{"mac":"00:03:52:ab:82:c2","rssi":-75},{"mac":"00:03:52:ab:82:c3","rssi":-75},{"mac":"00:03:52:ab:82:c0","rssi":-76},{"mac":"00:03:52:ab:82:c4","rssi":-76},{"mac":"9c:1c:12:c1:01:82","rssi":-82},{"mac":"9c:1c:12:c1:01:80","rssi":-85},{"mac":"c0:4a:00:05:4f:1c","rssi":-74},{"mac":"00:1b:11:f3:85:3b","rssi":-71},{"mac":"9c:1c:12:c1:01:81","rssi":-83},{"mac":"9c:1c:12:c1:01:83","rssi":-85},{"mac":"9c:1c:12:c1:01:85","rssi":-84},{"mac":"9c:1c:12:c1:01:84","rssi":-85},{"mac":"2c:41:38:ef:3b:f4","rssi":-84},{"mac":"2c:41:38:ef:3b:f1","rssi":-85}],"scan_id":18},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-75},{"mac":"00:03:52:ab:82:c6","rssi":-75},{"mac":"00:03:52:ab:82:c2","rssi":-75},{"mac":"00:03:52:ab:82:c3","rssi":-75},{"mac":"00:03:52:ab:82:c0","rssi":-75},{"mac":"00:03:52:ab:82:c4","rssi":-75},{"mac":"9c:1c:12:c1:01:82","rssi":-84},{"mac":"9c:1c:12:c1:01:80","rssi":-85},{"mac":"c0:4a:00:05:4f:1c","rssi":-72},{"mac":"00:1b:11:f3:85:3b","rssi":-67},{"mac":"9c:1c:12:c1:01:81","rssi":-84},{"mac":"9c:1c:12:c1:01:83","rssi":-84},{"mac":"9c:1c:12:c1:01:85","rssi":-85},{"mac":"9c:1c:12:c1:01:84","rssi":-84},{"mac":"2c:41:38:ef:3b:f4","rssi":-84},{"mac":"2c:41:38:ef:3b:f1","rssi":-85},{"mac":"2c:41:38:ef:3b:f6","rssi":-84}],"scan_id":19},{"ap_list":[{"mac":"00:03:52:ab:82:c5","rssi":-75},{"mac":"00:03:52:ab:82:c6","rssi":-75},{"mac":"00:03:52:ab:82:c2","rssi":-75},{"mac":"00:03:52:ab:82:c3","rssi":-75},{"mac":"00:03:52:ab:82:c0","rssi":-75},{"mac":"00:03:52:ab:82:c4","rssi":-76},{"mac":"9c:1c:12:c1:01:82","rssi":-84},{"mac":"9c:1c:12:c1:01:80","rssi":-85},{"mac":"c0:4a:00:05:4f:1c","rssi":-74},{"mac":"00:1b:11:f3:85:3b","rssi":-68},{"mac":"9c:1c:12:c1:01:81","rssi":-89},{"mac":"9c:1c:12:c1:01:83","rssi":-84},{"mac":"9c:1c:12:c1:01:85","rssi":-85},{"mac":"9c:1c:12:c1:01:84","rssi":-84},{"mac":"2c:41:38:ef:3b:f6","rssi":-84}],"scan_id":20}]
"""
