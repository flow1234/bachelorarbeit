import sqlite3 as lite
import sys

import Fingerprint

positions    = []
posWithRooms = []
dbValues     = []

g001    = ((393,144),(419,144))
g002    = ((299,120),(325,120),(299,141),(325,141))
g003    = ((393,184),(419,184),(393,205),(419,205))
g004    = ((299,184),(325,184),(299,205),(325,205))
g005    = ((393,237),(419,237),(393,258),(419,258))
g006    = ((299,237),(325,237),(299,258),(325,258))
g007    = ((393,289),(419,289),(393,310),(419,310))
g008    = ((299,289),(325,289),(299,310),(325,310))
g009    = ((393,348),(419,348),(406,378),(393,409),(419,409))
g010    = ((299,348),(325,348),(312,378),(299,409),(325,409))

toilets = ((295,62),(295,75),(328,68))
stairs  = ((408,68),(429,86),(408,103))
hall    = ((360,63),(360,94),(381,63),(381,94))

corU1   = ((15,34),(45,34),(75,34),(105,34),(135,34),(165,34))
corU2   = ((195,34),(225,34),(255,34),(285,34),(315,34),(345,34))
corU3   = ((375,34),(405,34),(435,34),(465,34),(495,34),(525,34))
corU4   = ((555,34),(585,34),(615,34),(645,34),(675,34))

corL1   = ((359,138),(359,168),(359,198),(359,228),(359,258))
corL2   = ((359,288),(359,318),(359,348),(359,378),(359,408))

posWithRooms.append(("g001",g001))
posWithRooms.append(("g002",g002))
posWithRooms.append(("g003",g003))
posWithRooms.append(("g004",g004))
posWithRooms.append(("g005",g005))
posWithRooms.append(("g006",g006))
posWithRooms.append(("g007",g007))
posWithRooms.append(("g008",g008))
posWithRooms.append(("g009",g009))
posWithRooms.append(("g010",g010))
posWithRooms.append(("toilets",toilets))
posWithRooms.append(("stairs",stairs))
posWithRooms.append(("hall",hall))
posWithRooms.append(("corU1",corU1))
posWithRooms.append(("corU2",corU2))
posWithRooms.append(("corU3",corU3))
posWithRooms.append(("corU4",corU4))
posWithRooms.append(("corL1",corL1))
posWithRooms.append(("corL2",corL2))


def loadPoints():

	try:
		con = lite.connect('DBFinal.sqlite')

		cur = con.cursor()

		cur.execute("SELECT * FROM probabilistic_fingerprints")

		data = cur.fetchall()

		for entry in data:
			_id = entry[0]
			x = entry[1]
			y = entry[2]
			positions.append((_id,Fingerprint.Point(x,y)))

	except lite.Error, e:

		print "Error %s:" % e.args[0]
		sys.exit(1)

	finally:

		if con:

			con.close()


def addRooms():

	for i in range(len(positions)):

		for j in range(len(posWithRooms)):

			for k in range(len(posWithRooms[j][1])):

				for l in range(len(posWithRooms[j][1][k])):

					if positions[i][1].x == posWithRooms[j][1][k][0] and positions[i][1].y == posWithRooms[j][1][k][1]:

						dbValues.append((positions[i][0],posWithRooms[j][0]))
						break
	

def writeChanges():

	try:

		con = lite.connect('DBFinal.sqlite')

		cur = con.cursor()

		for item in dbValues:

			cur.execute('UPDATE probabilistic_fingerprints SET room=? WHERE _id=?', (item[1], item[0]))

		con.commit()


	except lite.Error, e:

		print "Error %s:" % e.args[0]
		sys.exit(1)

	finally:

		if con:

			con.close()

		

def main():

	loadPoints()
	addRooms()
	writeChanges()


main()
