#!/bin/bash

# read macs available
patterns=`cat macs.txt`

cat $1".txt" | grep -F "${patterns}" > "RF_$1.txt"
