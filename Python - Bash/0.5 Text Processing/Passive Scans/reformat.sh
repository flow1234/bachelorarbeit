#!/bin/bash

# read macs available
patterns=`cat macs.txt`

# create tmp-file
echo -n "" > tmp

cat $1".log" | while read a;
do
	if [[ $a = *"Beacon"* ]]; then # Management-Frame / Beacon

		echo "$a" | cut -d " " -f 1,7,12 --output-delimiter "," | sed s/"BSSID:"/""/g | sed s/"dB"/""/g >> tmp

	elif [[ $a = *"SNAP"* ]]; then # Data-Frame

		mac=${a/*BSSID:}
		echo "$a" | cut -d " " -f 1,7 --output-delimiter "," | sed s/"dB"/""/g | sed 's/$/',${mac:0:17}'/' >> tmp
	fi
done

cat tmp | grep -F "${patterns}" > "RF_$1.txt"

rm tmp
