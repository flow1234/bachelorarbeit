import sys
import DistanceReasoner
import Fingerprint
import math
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

# positions (initial Point and direction) - Delete those outside of desired area
fps = []
#fps.append((Fingerprint.Point(358,93),'3'))  # 1
fps.append((Fingerprint.Point(359,322),'4')) # 2
fps.append((Fingerprint.Point(299,125),'2')) # 3
fps.append((Fingerprint.Point(359,202),'2')) # 4
#fps.append((Fingerprint.Point(406,36),'1'))  # 5
fps.append((Fingerprint.Point(326,124),'4')) # 6
fps.append((Fingerprint.Point(405,381),'2')) # 7
fps.append((Fingerprint.Point(319,295),'1')) # 8
fps.append((Fingerprint.Point(323,249),'2')) # 9
fps.append((Fingerprint.Point(417,190),'2')) # 10
fps.append((Fingerprint.Point(398,146),'2')) # 11
fps.append((Fingerprint.Point(354,406),'1')) # 12
fps.append((Fingerprint.Point(363,264),'4')) # 13
#fps.append((Fingerprint.Point(292,32),'1'))  # 14
fps.append((Fingerprint.Point(312,399),'2')) # 15
fps.append((Fingerprint.Point(329,380),'3')) # 16
fps.append((Fingerprint.Point(302,374),'1')) # 17
fps.append((Fingerprint.Point(326,195),'3')) # 18
#fps.append((Fingerprint.Point(591,31),'2'))  # 19

maximum_k = 13
scaleMeter = 13.0

# arrays with results for each value of k
# [kValue][meanPosError, maxPosError, minPosError, stdDev]
# CO ~ Consider a Min Value
# NO ~ No Min Value
resultsActive_CO = [[0 for x in range(4)] for x in range(maximum_k)]
resultsActive_NO = [[0 for x in range(4)] for x in range(maximum_k)]

resultsPassive_CO = [[0 for x in range(4)] for x in range(maximum_k)]
resultsPassive_NO = [[0 for x in range(4)] for x in range(maximum_k)]


def readTxtFile(scantype,fpNr):
	""" get all scans from .txt-File """

	if scantype == 0: # active

		f = open("Data/RF_afp"+str(fpNr)+".txt",'r')

	else: # passive

		f = open("Data/RF_bfp"+str(fpNr)+".txt",'r')

	measures_raw = []
	measures_final = []
	macs = []

	for line in f:

		measures_raw.append(Fingerprint.APInfo(line.split(',')[2].rstrip('\n'),int(line.split(',')[1])))

	for measure in measures_raw:

		if measure.mac[0:14] not in macs:

			macs.append(measure.mac[0:14])

	for mac in macs:

		sumRSSI = 0
		ctr = 0

		for measure in measures_raw:

			if measure.mac[0:14] == mac:

				sumRSSI += measure.rssi
				ctr += 1

		measures_final.append(Fingerprint.APInfo(mac,(sumRSSI/ctr)))

	return measures_final


def getValues():
	""" Calculate Mean/Max Positioning Error and Standard Deviation for different Values of K """

	distancesActive_CO   = [[0 for x in range(len(fps))] for x in range(maximum_k)] 
	distancesActive_NO   = [[0 for x in range(len(fps))] for x in range(maximum_k)]

	distancesPassive_CO  = [[0 for x in range(len(fps))] for x in range(maximum_k)] 
	distancesPassive_NO  = [[0 for x in range(len(fps))] for x in range(maximum_k)]

	# for every kValue < 14
	for i in range(maximum_k):

		# for every Point
		for j in range(len(fps)):

			# Read the .txt-File
			activeScan  = readTxtFile(0,j+1)
			passiveScan = readTxtFile(1,j+1)

			# Initialize a Distance Reasoner based on kValue and Direction of certain Point
			# DistanceReasoner(Direction,kValue,weighted,minVal)
			# Direction: 0    -> No Direction     | 1-4   -> Direction
			# kValue:    1-15
			# weighted:  True -> use weighted kNN | False -> use centroids
			# minVal:    True -> compare missing  | False -> ignore missing

			if sys.argv[2] == '1': # MinVal

				ddr_CO = DistanceReasoner.Deterministic(fps[j][1],i+1,True,True)
				ddr_NO = DistanceReasoner.Deterministic(fps[j][1],i+1,True,False)

			elif sys.argv[2] == '2': # weighted kNN

				ddr_CO = DistanceReasoner.Deterministic(fps[j][1],i+1,True,True)
				ddr_NO = DistanceReasoner.Deterministic(fps[j][1],i+1,False,True)

			elif sys.argv[2] == '3': # Direction

				ddr_CO = DistanceReasoner.Deterministic(fps[j][1],i+1,True,True)
				ddr_NO = DistanceReasoner.Deterministic('0',i+1,True,True)

			# Get the Approximate Position
			activePt_CO  = ddr_CO.getApproximatePosition(activeScan)
			activePt_NO  = ddr_NO.getApproximatePosition(activeScan)

			passivePt_CO = ddr_CO.getApproximatePosition(passiveScan)
			passivePt_NO = ddr_NO.getApproximatePosition(passiveScan)

			# Calculate Euclidean Distances
			distancesActive_CO[i][j] = math.sqrt(math.pow(fps[j][0].x-activePt_CO.x,2)+math.pow(fps[j][0].y-activePt_CO.y,2))
			distancesActive_NO[i][j] = math.sqrt(math.pow(fps[j][0].x-activePt_NO.x,2)+math.pow(fps[j][0].y-activePt_NO.y,2))

			distancesPassive_CO[i][j] = math.sqrt(math.pow(fps[j][0].x-passivePt_CO.x,2)+math.pow(fps[j][0].y-passivePt_CO.y,2))
			distancesPassive_NO[i][j] = math.sqrt(math.pow(fps[j][0].x-passivePt_NO.x,2)+math.pow(fps[j][0].y-passivePt_NO.y,2))


	# Calculate the desired Values
	for i in range(maximum_k):

		# Average Positioning Error
		meanPosError_active_CO  = sum(distancesActive_CO[i])/len(distancesActive_CO[i])
		meanPosError_active_NO  = sum(distancesActive_NO[i])/len(distancesActive_NO[i])

		meanPosError_passive_CO = sum(distancesPassive_CO[i])/len(distancesPassive_CO[i])
		meanPosError_passive_NO = sum(distancesPassive_NO[i])/len(distancesPassive_NO[i])

		# Maximum Positioning Error
		maxPosError_active_CO = max(distancesActive_CO[i])
		maxPosError_active_NO = max(distancesActive_NO[i])

		maxPosError_passive_CO = max(distancesPassive_CO[i])
		maxPosError_passive_NO = max(distancesPassive_NO[i])

		# Minimum Positioning Error
		minPosError_active_CO = min(distancesActive_CO[i])
		minPosError_active_NO = min(distancesActive_NO[i])

		minPosError_passive_CO = min(distancesPassive_CO[i])
		minPosError_passive_NO = min(distancesPassive_NO[i])

		# Standard Deviation
		s2_active_CO  = 0
		s2_active_NO  = 0

		s2_passive_CO = 0
		s2_passive_NO = 0

		for j in range(len(distancesActive_CO[i])):

			s2_active_CO += math.pow(distancesActive_CO[i][j]-meanPosError_active_CO,2)
			s2_active_NO += math.pow(distancesActive_NO[i][j]-meanPosError_active_NO,2)

			s2_passive_CO += math.pow(distancesPassive_CO[i][j]-meanPosError_passive_CO,2)
			s2_passive_NO += math.pow(distancesPassive_NO[i][j]-meanPosError_passive_NO,2)
		
		standardDeviation_active_CO = math.sqrt(s2_active_CO/len(fps))
		standardDeviation_active_NO = math.sqrt(s2_active_NO/len(fps))

		standardDeviation_passive_CO = math.sqrt(s2_passive_CO/len(fps))
		standardDeviation_passive_NO = math.sqrt(s2_passive_NO/len(fps))

		# Store Results
		resultsActive_CO[i][0] = meanPosError_active_CO / scaleMeter
		resultsActive_CO[i][1] = maxPosError_active_CO / scaleMeter
		resultsActive_CO[i][2] = minPosError_active_CO / scaleMeter
		resultsActive_CO[i][3] = standardDeviation_active_CO / scaleMeter

		resultsActive_NO[i][0] = meanPosError_active_NO / scaleMeter
		resultsActive_NO[i][1] = maxPosError_active_NO / scaleMeter
		resultsActive_NO[i][2] = minPosError_active_NO / scaleMeter
		resultsActive_NO[i][3] = standardDeviation_active_NO / scaleMeter

		resultsPassive_CO[i][0] = meanPosError_passive_CO / scaleMeter
		resultsPassive_CO[i][1] = maxPosError_passive_CO / scaleMeter
		resultsPassive_CO[i][2] = minPosError_passive_CO / scaleMeter
		resultsPassive_CO[i][3] = standardDeviation_passive_CO / scaleMeter

		resultsPassive_NO[i][0] = meanPosError_passive_NO / scaleMeter
		resultsPassive_NO[i][1] = maxPosError_passive_NO / scaleMeter
		resultsPassive_NO[i][2] = minPosError_passive_NO / scaleMeter
		resultsPassive_NO[i][3] = standardDeviation_passive_NO / scaleMeter


def plotResults(inputmode):

	kValues = []
	mode = inputmode

	for i in range(maximum_k):

		kValues.append((i+1))		

	meanPosErrors_active_CO  = []
	meanPosErrors_active_NO  = []
	meanPosErrors_passive_CO = []
	meanPosErrors_passive_NO = []

	maxPosErrors_active_CO  = []
	maxPosErrors_active_NO  = []
	maxPosErrors_passive_CO = []
	maxPosErrors_passive_NO = []

	minPosErrors_active_CO  = []
	minPosErrors_active_NO  = []
	minPosErrors_passive_CO = []
	minPosErrors_passive_NO = []

	stdDev_active_CO  = []
	stdDev_active_NO  = []
	stdDev_passive_CO = []
	stdDev_passive_NO = []
	
	for i in range(len(resultsActive_CO)):

		meanPosErrors_active_CO.append(resultsActive_CO[i][0])
		meanPosErrors_active_NO.append(resultsActive_NO[i][0])
		meanPosErrors_passive_CO.append(resultsPassive_CO[i][0])
		meanPosErrors_passive_NO.append(resultsPassive_NO[i][0])

		maxPosErrors_active_CO.append(resultsActive_CO[i][1])
		maxPosErrors_active_NO.append(resultsActive_NO[i][1])
		maxPosErrors_passive_CO.append(resultsPassive_CO[i][1])
		maxPosErrors_passive_NO.append(resultsPassive_NO[i][1])

		minPosErrors_active_CO.append(resultsActive_CO[i][2])
		minPosErrors_active_NO.append(resultsActive_NO[i][2])
		minPosErrors_passive_CO.append(resultsPassive_CO[i][2])
		minPosErrors_passive_NO.append(resultsPassive_NO[i][2])

		stdDev_active_CO.append(resultsActive_CO[i][3])
		stdDev_active_NO.append(resultsActive_NO[i][3])
		stdDev_passive_CO.append(resultsPassive_CO[i][3])
		stdDev_passive_NO.append(resultsPassive_NO[i][3])


	for i in range(maximum_k):

		print "K=%d" % (i+1)
		print "Active_CO : min=%.2f m, mean=%.2f m, max=%.2f m, stdDev=%.2f m" % (minPosErrors_active_CO[i],meanPosErrors_active_CO[i],maxPosErrors_active_CO[i],stdDev_active_CO[i])
		print "Active_NO : min=%.2f m, mean=%.2f m, max=%.2f m, stdDev=%.2f m" % (minPosErrors_active_NO[i],meanPosErrors_active_NO[i],maxPosErrors_active_NO[i],stdDev_active_NO[i])
		print "Passive_CO : min=%.2f m, mean=%.2f m, max=%.2f m, stdDev=%.2f m" % (minPosErrors_passive_CO[i],meanPosErrors_passive_CO[i],maxPosErrors_passive_CO[i],stdDev_passive_CO[i])
		print "Passive_NO : min=%.2f m, mean=%.2f m, max=%.2f m, stdDev=%.2f m" % (minPosErrors_passive_NO[i],meanPosErrors_passive_NO[i],maxPosErrors_passive_NO[i],stdDev_passive_NO[i])

	plt.xlabel('k')
	plt.ylabel('Meter')
	plt.xlim([0,14])
	plt.ylim([0,15.0])
	plt.grid()

	# SMARTPOS Values
	SPkNN_meanCO = [1.93,1.66,1.57,1.54,1.49,1.51,1.49,1.46,1.45,1.46,1.47,1.5,1.47]
	SPkNN_meanNO = [1.93,1.68,1.59,1.56,1.52,1.54,1.53,1.48,1.475,1.49,1.5,1.57,1.555]
	SPkNN_devCO = [1.27,1.03,0.98,0.95,0.9,0.9,0.87,0.84,0.8,0.79,0.77,0.78,0.79]
	SPkNN_devNO = [1.27,1.04,1,0.96,0.9,0.91,0.88,0.86,0.81,0.8,0.77,0.8,0.8]
	SPkNN_maxCO = [7.1,4.6,4,3.7,3.9,4.1,3.7,3.6,3.3,3.4,3.3,3.24,3.2]
	SPkNN_maxNO = [7.1,4.6,4.4,3.7,3.92,4.1,3.7,3.78,3.3,3.4,3.3,3.3,3.2]

	SPmin_meanCO = [1.92,1.67,1.56,1.51,1.49,1.51,1.48,1.46,1.45,1.46,1.48,1.5,1.47]
	SPmin_meanNO = [2.12,1.73,1.59,1.53,1.41,1.43,1.38,1.35,1.31,1.35,1.38,1.35,1.37]
	SPmin_devCO = [1.27,1.04,0.98,0.94,0.9,0.9,0.86,0.84,0.8,0.79,0.77,0.78,0.79]
	SPmin_devNO = [1.38,1.23,1.1,0.98,0.91,0.92,0.87,0.82,0.78,0.77,0.74,0.75,0.76]
	SPmin_maxCO = [7.1,4.6,4,3.7,3.9,4.1,3.6,3.53,3.37,3.43,3.3,3.27,3.1]
	SPmin_maxNO = [6.4,5.4,4.97,4.5,3.5,3.92,3.8,3.3,3.4,3.5,3.6,3.48,3.52]

	SPdir_meanCO = [1.91,1.5,1.2,1.17,1.19,1.25,1.28,1.35,1.35,1.42,1.45,1.49,1.53]
	SPdir_meanNO = [2.11,1.72,1.58,1.51,1.41,1.44,1.38,1.36,1.32,1.35,1.38,1.36,1.38]
	SPdir_devCO = [1.3,0.86,0.75,0.67,0.63,0.58,0.66,0.69,0.68,0.74,0.8,0.81,0.8]
	SPdir_devNO = [1.38,1.21,1.1,0.98,0.9,0.92,0.87,0.82,0.79,0.77,0.73,0.74,0.75]
	SPdir_maxCO = [5.2,5.1,4,2.7,2.6,2.8,3.21,3,3.3,3.55,3.6,3.9,4.1]
	SPdir_maxNO = [6.4,5.4,4.8,4.5,3.5,3.94,3.8,3.3,3.4,3.46,3.6,3.48,3.5]

	if (sys.argv[1] == '1'): # Show Standard Deviation

		plt.ylabel('Standardabweichung (in Meter)')

		if (sys.argv[2] == '1'): # minValue
			plt.plot(kValues, stdDev_active_CO, linewidth=3.,color='r',alpha=.8)
			plt.plot(kValues, stdDev_active_NO,linewidth=3.,color='r',linestyle='dashed')
			plt.plot(kValues, SPmin_devCO, linewidth=3.,color='b',alpha=.8)
			plt.plot(kValues, SPmin_devNO, linewidth=3.,color='b',linestyle='dashed')
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Mit MinWert")
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Ohne MinWert",linestyle='dashed')
			plt.ylim([0.7,3.15])
			plt.legend(framealpha=0.3)
			plt.savefig('SPMinVal-StdDev.pdf', format='pdf', dpi=1200)

		if (sys.argv[2] == '2'): # wKNN
			plt.plot(kValues, stdDev_active_CO, linewidth=3.,color='r',alpha=.8)
			plt.plot(kValues, stdDev_active_NO, linewidth=3.,color='r',linestyle='dashed')
			plt.plot(kValues, SPkNN_devCO, linewidth=3.,color='b',alpha=.8)
			plt.plot(kValues, SPkNN_devNO, linewidth=3.,color='b',linestyle='dashed')
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Gewichtet")
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Nicht Gewichtet",linestyle='dashed')
			plt.ylim([0.7,2.5])
			plt.legend(framealpha=0.3)
			plt.savefig('SPWkNN-StdDev.pdf', format='pdf', dpi=1200)

		if (sys.argv[2] == '3'): # Orientierung
			plt.plot(kValues, stdDev_active_CO, linewidth=3.,color='r',alpha=.8)
			plt.plot(kValues, stdDev_active_NO, linestyle='dashed', linewidth=3.,color='r')
			plt.plot(kValues, SPdir_devCO, linewidth=3.,color='b',alpha=.8)
			plt.plot(kValues, SPdir_devNO, linewidth=3.,color='b',linestyle='dashed')
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Mit Orientierung")
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Ohne Orientierung",linestyle='dashed')
			plt.ylim([0.5,2.6])
			plt.legend(framealpha=0.3)
			plt.savefig('SPDIR-StdDev.pdf', format='pdf', dpi=1200)

	if (sys.argv[1] == '2'): # Show Mean Pos Error Diagram

		plt.ylabel('Durchschnittlicher Positionierungsfehler (in Meter)')

		if (sys.argv[2] == '1'): # minValue
			plt.plot(kValues, meanPosErrors_active_CO, linewidth=3.,color='r',alpha=.8)
			plt.plot(kValues, meanPosErrors_active_NO, linewidth=3.,color='r',linestyle='dashed')
			plt.plot(kValues, SPmin_meanCO, linewidth=3.,color='b',alpha=.8)
			plt.plot(kValues, SPmin_meanNO, linewidth=3.,color='b',linestyle='dashed')
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Mit MinWert")
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Ohne MinWert",linestyle='dashed')
			plt.ylim([1.25,4.5])
			plt.legend(framealpha=0.3)
			plt.savefig('SPMinVal-mean.pdf', format='pdf', dpi=1200)

		if (sys.argv[2] == '2'): # weighted kNN
			plt.plot(kValues, meanPosErrors_active_CO, linewidth=3.,color='r',alpha=.8)
			plt.plot(kValues, meanPosErrors_active_NO, linewidth=3.,color='r',linestyle='dashed')
			plt.plot(kValues, SPkNN_meanCO, linewidth=3.,color='b',alpha=.8)
			plt.plot(kValues, SPkNN_meanNO, linewidth=3.,color='b',linestyle='dashed')
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Gewichtet")
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Nicht gewichtet",linestyle='dashed')
			plt.ylim([1.4,3.3])
			plt.legend(framealpha=0.3)
			plt.savefig('SPWkNN-mean.pdf', format='pdf', dpi=1200)

		if (sys.argv[2] == '3'): # Orientierung
			plt.plot(kValues, meanPosErrors_active_CO, linewidth=3.,color='r',alpha=.8)
			plt.plot(kValues, meanPosErrors_active_NO, linestyle='dashed', linewidth=3.,color='r')
			plt.plot(kValues, SPdir_meanCO, linewidth=3.,color='b',alpha=.8)
			plt.plot(kValues, SPdir_meanNO, linewidth=3.,color='b',linestyle='dashed')
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Mit Orientierung")
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Ohne Orientierung",linestyle='dashed')
			plt.ylim([1.1,3])
			plt.legend(framealpha=0.3)
			plt.savefig('SPDIR-mean.pdf', format='pdf', dpi=1200)

	if (sys.argv[1] == '3'): # Show Max Pos Error Diagram

		plt.ylabel('Maximaler Positionierungsfehler (in Meter)')

		if (sys.argv[2] == '1'): # minValue
			plt.plot(kValues, maxPosErrors_active_CO, linewidth=3.,color='r',alpha=.8)
			plt.plot(kValues, maxPosErrors_active_NO, linewidth=3.,color='r',linestyle='dashed')
			plt.plot(kValues, SPmin_maxCO, linewidth=3.,color='b',alpha=.8)
			plt.plot(kValues, SPmin_maxNO, linewidth=3.,color='b',linestyle='dashed')
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Mit MinWert")
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Ohne MinWert",linestyle='dashed')
			plt.ylim([3,11])
			plt.legend(framealpha=0.3)
			plt.savefig('SPMinVal-max.pdf', format='pdf', dpi=1200)

		if (sys.argv[2] == '2'): # weighted kNN
			plt.plot(kValues, maxPosErrors_active_CO, linewidth=3.,color='r',alpha=.8)
			plt.plot(kValues, maxPosErrors_active_NO, linewidth=3.,color='r',linestyle='dashed')
			plt.plot(kValues, SPkNN_maxCO, linewidth=3.,color='b',alpha=.8)
			plt.plot(kValues, SPkNN_maxNO, linewidth=3.,color='b',linestyle='dashed')
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Gewichtet")
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Nicht Gewichtet",linestyle='dashed')
			plt.ylim([3,11])
			plt.legend(framealpha=0.3)
			plt.savefig('SPWkNN-max.pdf', format='pdf', dpi=1200)

		if (sys.argv[2] == '3'): # Orientierung
			plt.plot(kValues, maxPosErrors_active_CO, linewidth=3.,color='r',alpha=.8)
			plt.plot(kValues, maxPosErrors_active_NO, linewidth=3.,color='r',linestyle='dashed')
			plt.plot(kValues, SPdir_maxCO, linewidth=3.,color='b',alpha=.8)
			plt.plot(kValues, SPdir_maxNO, linewidth=3.,color='b',linestyle='dashed')
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Mit Orientierung")
			plt.plot([-1],[-1], linewidth=3.,color='k',label="Ohne Orientierung",linestyle='dashed')
			plt.ylim([2.5,9])
			plt.legend(framealpha=0.3)
			plt.savefig('SPDIR-max.pdf', format='pdf', dpi=1200)


	plt.show()


def showHelp():
	""" show Help """

	print "\nError. Need Parameter for mode:\n"
	print "Parameter 1:"
	print "1 - Standardabweichung"
	print "2 - Mean Positionsfehler"
	print "3 - Max Positionsfehler"
	print "Parameter 2:"
	print "1 - minValue"
	print "2 - weighted KNN"
	print "3 - Orientierung"


def main():
	""" Main Function """

	if len(sys.argv) != 3 or int(sys.argv[1]) < 1 or int(sys.argv[1]) > 3 or int(sys.argv[2]) < 1 or int(sys.argv[2])  > 3:

		showHelp()

	else:

		getValues()
		plotResults(int(sys.argv[1]))

main()
