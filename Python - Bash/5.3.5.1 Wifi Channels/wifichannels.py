from matplotlib import pyplot as plt
from operator import itemgetter
import numpy as np

doc = []
macs = []
channels = []
stations = []
ctrlist = []

def readDoc():
	
	f = open('APChannels.txt','r')
	
	for line in f:

		doc.append(line.rstrip('\n'))


def extractData():

	for i in range(len(doc)):

		if (i%3) == 0:

			if doc[i] not in macs:

				macs.append(doc[i])
				stations.append((doc[i],int(doc[i+1])))

				if doc[i+1] not in channels:

					channels.append(doc[i+1])


def handleData():
	
	stations.sort(key=itemgetter(0))
	stations.sort(key=itemgetter(1))
	
	ch1ctr  = 0
	ch5ctr  = 0
	ch6ctr  = 0
	ch9ctr  = 0
	ch10ctr = 0
	ch11ctr = 0
	ch13ctr = 0

	print "\n__Macs__"
	for item in stations:

		print "Mac %s on Channel %s" % (item[0],item[1])

		if   item[1] == 1: ch1ctr += 1
		elif item[1] == 5: ch5ctr += 1
		elif item[1] == 6: ch6ctr += 1
		elif item[1] == 9: ch9ctr += 1
		elif item[1] == 10: ch10ctr += 1
		elif item[1] == 11: ch11ctr += 1
		elif item[1] == 13: ch13ctr += 1


	print "\n__Distribution__"
	print "%d broadcast on Channel 1" % ch1ctr
	print "%d broadcast on Channel 5" % ch5ctr
	print "%d broadcast on Channel 6" % ch6ctr
	print "%d broadcast on Channel 9" % ch9ctr
	print "%d broadcast on Channel 10" % ch10ctr
	print "%d broadcast on Channel 11" % ch11ctr
	print "%d broadcast on Channel 13" % ch13ctr

	channels.sort(key=int)

	ctrlist.append(ch1ctr)
	ctrlist.append(ch5ctr)
	ctrlist.append(ch6ctr)
	ctrlist.append(ch9ctr)
	ctrlist.append(ch10ctr)
	ctrlist.append(ch11ctr)
	ctrlist.append(ch13ctr)


def plotResults():

	ind = np.arange(len(channels))
	width = 0.99

	fig, ax = plt.subplots()
	rects1 = ax.bar(ind, ctrlist, width, color='r')

	ax.set_ylabel('Anzahl an Access Points')
	ax.set_xlabel('Kanal')
	#ax.set_title('Access Point Channel Distribution')
	ax.set_xticks(ind+width-(width/2))
	ax.set_xticklabels(channels)

	plt.savefig('WifiDestribution.pdf', format='pdf', dpi=1200)
	plt.show()

def main():
	readDoc()
	extractData()
	handleData()
	plotResults()



main()
