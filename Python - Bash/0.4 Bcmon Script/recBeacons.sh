#!/bin/bash

LOG_DIR=/extSdCard			# storage directory (extSdCard)

TS_START=`date +%s` 		# init timestamp
CH=1						# init channel

REC_LENGTH=2				# length of tcpdump (how long till switch channel)
DURATION=20					# duration of log (in seconds)

echo "Recording Beacon" > $LOG_DIR/$1.log	# write document

iwconfig wlan0 channel $CH

while true; do
	TS_CUR=`date +%s` 			# current timestamp
	TIME_PASSED=`expr $TS_CUR - $TS_START` 	# time passed (current_ts - start_ts)

	timeout -t $REC_LENGTH tcpdump -i wlan0 -e -vvv type mgt subtype beacon or type data >> $LOG_DIR/$1.log


	if [ $CH -eq 1 ]			# select channel
	then		
		CH=6
	elif [ $CH -eq 6 ]
	then
		CH=11
	elif [ $CH -eq 11 ]
	then
		CH=1
	fi

	iwconfig wlan0 channel $CH		# switch channel

	
	if [ $TIME_PASSED -gt $DURATION ] 	# if time_passed > duration -> exit
	then
		break
	fi
done

echo "record finished."
date
echo "file saved to $LOG_DIR/$1.log"
