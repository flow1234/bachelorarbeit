import DistanceReasoner
import Fingerprint
import sys
import math
import matplotlib.pyplot as plt

maximum_k = 10
scaleMeter = 13.0

distances = [[0 for x in range(332)] for x in range(maximum_k)]

def jackknife():

	ddr = DistanceReasoner.Deterministic('0',1)
	trainingsData = ddr.getFingerprints()
	testObject = []

	for k in range(maximum_k):

		ddrT = DistanceReasoner.Deterministic('0',(k+1))

		for i in range(len(trainingsData)):

			testObject.append(trainingsData.pop(0))
	
			ddrT.setFingerprints(trainingsData)

			temp = ddrT.getApproximatePosition(testObject[0].wifi_scans[0].ap_infos)
	
			distances[k][i] = math.sqrt(math.pow(testObject[0].x-temp.x,2)+math.pow(testObject[0].y-temp.y,2))

			trainingsData.append(testObject.pop(0))

		print "K: %d" % (k+1) 

	for k in range(maximum_k):

		print "K = %d , Average Deviation = %d" % ((k+1) , sum(distances[k])/len(distances[k]))


def plotResults():

	resultsMax = []
	resultsMean = []
	resultsMin = []
	stdDevs = []
	kValues = []

	for k in range(maximum_k):

		kValues.append(k+1)
		resultsMax.append(max(distances[k]) / scaleMeter)
		resultsMean.append((sum(distances[k])/len(distances[k])) / scaleMeter)
		resultsMin.append(min(distances[k]) / scaleMeter)
		
		s2 = 0

		for i in range(len(distances[k])):

			s2 += math.pow(distances[k][i]-(sum(distances[k])/len(distances[k])),2)

		stdDevs.append(math.sqrt(s2/len(distances[k])) / scaleMeter)

	plt.xlabel('k')
	plt.ylabel('Positionierungssfehler (in Meter)')
	plt.xlim([0,11])
	plt.grid()

	if sys.argv[1] == '1':
	
		plt.ylim([-0.5,16.0])
		plt.plot(kValues, resultsMean, label="Durchschnitt", linewidth=3.,color='r')
		plt.plot(kValues, resultsMax, label="Minimum/Maximum", linewidth=3.,color='k',linestyle='dashed')
		plt.plot(kValues, resultsMin, linewidth=3.,color='k',linestyle='dashed')
		plt.legend()
		plt.savefig('Jackknife-Pos.pdf', format='pdf', dpi=1200)

	elif sys.argv[1] == '2':

		plt.ylim([1.2,2.1])
		plt.plot(kValues, stdDevs, linewidth=3.,color='b')
		plt.ylabel('Standardabweichung (in Meter)')
		plt.savefig('Jackknife-StdDev.pdf', format='pdf', dpi=1200)

	plt.show()

	for i in range(len(stdDevs)):

		print stdDevs[i]

def main():

	jackknife()
	plotResults()

main()
