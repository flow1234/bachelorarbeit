import sqlite3 as lite
import math
import Fingerprint
from operator import itemgetter

class Deterministic(object):	

	# Main constructor
	def __init__(self, direction, kValue):
	
		self.direction = direction
		self.kValue = kValue
		self.fingerprints = []
		self.loadFingerprintData()


	# Get Fingerprints from DB
	def getFingerprints(self):

		return self.fingerprints

	def setFingerprints(self,fps):

		self.fingerprints = fps


	def getApproximatePosition(self, measures):
		""" calculate an approximate position based on the algorithm suggested from SmartPos """

		neighbors = self.getKNearestNeighbors(measures)
		distances = [0] * self.kValue
		weights   = [0] * self.kValue

		# get Distances
		for i in range(len(neighbors)):

			distances[i] = self.euclideanDistance(neighbors[i],measures)

			if distances[i] == 0:

				distances[i] += 1
	
		# assign Weights
		for i in range(len(neighbors)):

			tempWeight = 0

			for j in range(len(neighbors)):

				tempWeight += 1/distances[j]

			weights[i] = 1/(distances[i] * tempWeight);

		x = 0
		y = 0

		# approximate x and y
		for i in range(len(neighbors)):
		
			x += neighbors[i].x * weights[i]
			y += neighbors[i].y * weights[i]

		approxPos = Fingerprint.Point(x,y)

		return approxPos


	def getKNearestNeighbors(self, measures):
		""" get K-Nearest Neighbors based on their euclidean distances """

		distances = []

		for i in range(len(self.fingerprints)):

			dist = self.euclideanDistance(self.fingerprints[i],measures)

			if dist >= 0: # if dist < 0  -> no apinfo hit

				distances.append((self.fingerprints[i],dist))

		distances.sort(key=itemgetter(1))

		nearest_neighbors = []

		for i in range(self.kValue):

			nearest_neighbors.append(distances[i][0])

		return nearest_neighbors


	def euclideanDistance(self, fp, measures):
		""" Euclidean Distance Function """

		distance = -1
		minVal = -100

		for measured_ap_info in measures:

			hit = False

			for wifi_scan in fp.wifi_scans:

				for db_ap_info in wifi_scan.ap_infos:

					if db_ap_info.mac == measured_ap_info.mac[0:14]:

						if distance < 0:

							distance = 0

						hit = True

						distance += math.pow(measured_ap_info.rssi - db_ap_info.rssi,2)

			if hit == False:

				distance += math.pow(measured_ap_info.rssi - minVal,2)

		if distance > 0:

			distance = math.sqrt(distance)

		return distance


	def loadFingerprintData(self):
		""" Load SQLite-Data from Det-DB """

		try:

			con = lite.connect('Database/DBFinal.sqlite')

			cur = con.cursor()

			if self.direction == '0':

				cur.execute("SELECT * FROM deterministic_fingerprints")				

			else:

				cur.execute("SELECT * FROM deterministic_fingerprints WHERE direction=?", self.direction)

			data = cur.fetchall()

			for entry in data:

				_id = entry[0]
				x   = entry[1]
				y   = entry[2]
				d   = entry[3]
				s   = entry[4]
				r   = entry[5]

				self.fingerprints.append(Fingerprint.ProbData(_id,x,y,d,s,r))

		except lite.Error, e:

			print "Error %s:" % e.args[0]
			sys.exit(1)

		finally:

			if con:

				con.close()
